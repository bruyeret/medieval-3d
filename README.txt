Pour lancer le projet :

Dépendances autres que celles des TPs :
- pybullet
- scipy
(installables via pip)

Pour exécuter :
- Se placer dans le dossier src
- Exécuter le fichier main.py

Les contrôles sont affichés sur l'écran une fois le projet lancé.
Les voici réécris au cas où :
- ZQSD pour se déplacer
- Mouvement de souris pour la vue
- Tabulation pour passer du mode collisions au mode vol
- Espace pour sauter ou s'envoler
- Shift pour descendre en mode vol
- Clique droit pour tuer les ennemis à proximité.
  (Les ennemis sont très grands par rapport au monde, il faut être à leurs pieds pour les tuer)
- Clique gauche pour lancer un cube

Elements implémentés dans le projet :
- Sol en volume généré avec un bruit de Perlin
- Multitexturing avec des transitions entre 2 textures
- Eau "cartoonesque" avec simulation de vagues
- Ajout de modèles statiques via un éditeur de scène
- Gestion de modèles animés par keyframes
- Bulles de sang
- Collisions avec le module pybullet
- Ragdoll lors du décès des ennemis
- Système de chunk permettant d'afficher ou non des bouts de terrains selon la distance au joueur
- Lumière via le modèle de Phong