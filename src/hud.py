import numpy as np
import OpenGL.GL as GL


class HUD:
    """Heads up display over 3D scene"""
    
    def __init__(self, shader, texture):
        """Intialise the HUD with a single shader and a texture"""
        self.shader = shader
        self.texture = texture
        # number of quads in the HUD
        self.nquads = 1
        # 6 positions per quad as vec2
        self.position = np.reshape(np.array([], np.float32), (0, 2))
        # 6 uvs per quad as vec2
        self.uv = np.reshape(np.array([], np.float32), (0, 2))
        # texture location in fragment shader
        self.tex_loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map')
        
        # creating vao and vbos
        self.vao = GL.glGenVertexArrays(1)
        GL.glBindVertexArray(self.vao)
        
        self.vbo_pos = GL.glGenBuffers(1)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbo_pos)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self.position, GL.GL_DYNAMIC_DRAW)
        GL.glEnableVertexAttribArray(0)
        GL.glVertexAttribPointer(0, 2, GL.GL_FLOAT, False, 0, None)
        
        self.vbo_uv = GL.glGenBuffers(1)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbo_uv)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self.uv, GL.GL_DYNAMIC_DRAW)
        GL.glEnableVertexAttribArray(1)
        GL.glVertexAttribPointer(1, 2, GL.GL_FLOAT, False, 0, None)
        
        GL.glBindVertexArray(0)

    def add_quad(self, positions, uvs):
        """4 positions and 4 uvs"""
        positions = np.reshape(np.array(positions), (4, 2))
        positions = (positions[0], positions[1], positions[2], positions[1], positions[2], positions[3])
        positions = np.reshape(np.array(positions), (6, 2))
        self.position = np.append(self.position, positions, axis=0)
        self.position = np.array(self.position, copy=False, dtype=np.float32)
        
        uvs = np.reshape(np.array(uvs), (4, 2))
        uvs = (uvs[0], uvs[1], uvs[2], uvs[1], uvs[2], uvs[3])
        uvs = np.reshape(np.array(uvs), (6, 2))
        self.uv = np.append(self.uv, uvs, axis=0)
        self.uv = np.array(self.uv, copy=False, dtype=np.float32)
        
        GL.glBindVertexArray(self.vao)
        
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbo_pos)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self.position, GL.GL_DYNAMIC_DRAW)
        
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vbo_uv)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self.uv, GL.GL_DYNAMIC_DRAW)
        
        GL.glBindVertexArray(0)
        self.nquads += 1

    def draw(self):
        GL.glUseProgram(self.shader.glid)
        GL.glDisable(GL.GL_DEPTH_TEST)
        GL.glDisable(GL.GL_CULL_FACE)
        
        GL.glBindVertexArray(self.vao)
        
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(self.tex_loc, 0)
        
        GL.glDrawArrays(GL.GL_TRIANGLES, 0, 6*self.nquads)
        
        GL.glEnable(GL.GL_CULL_FACE)
        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glBindVertexArray(0)
