# Python built-in modules
import os                           # os function, i.e. checking file status
import sys                          # for sys.exit
from itertools import cycle         # allows easy circular choice list

# External, non built-in modules
import OpenGL.GL as GL              # standard Python OpenGL wrapper
import glfw                         # lean window system wrapper for OpenGL
import numpy as np                  # all matrix manipulations & OpenGL args
import pybullet as pb
from ctypes import c_void_p
from scipy.spatial import ConvexHull

# our transform functions
from transform import Trackball, identity, vec, quaternion_matrix

from PIL import Image               # load images for textures


from transform import lerp
from bisect import bisect_left      # search sorted keyframe lists

from transform import quaternion_slerp, translate, scale, get_TRS_from_matrix


class KeyFrames:
    """ Stores keyframe pairs for any value type with interpolation_function"""

    def __init__(self, time_value_pairs, interpolation_function=lerp):
        if isinstance(time_value_pairs, dict):  # convert to list of pairs
            time_value_pairs = time_value_pairs.items()
        keyframes = sorted(((key[0], key[1]) for key in time_value_pairs))
        self.times, self.values = zip(*keyframes)  # pairs list -> 2 lists
        self.interpolate = interpolation_function

    def value(self, time):
        """ Computes interpolated value from keyframes, for a given time """
        # 1. ensure time is within bounds else return boundary keyframe
        if (time <= self.times[0]):
            return self.values[0]
        if (time >= self.times[-1]):
            return self.values[-1]

        # 2. search for closest index entry in self.times, using bisect_left function
        index = bisect_left(self.times, time)

        # 3. using the retrieved index, interpolate between the two neighboring values
        # in self.values, using the initially stored self.interpolate function
        fraction = (time - self.times[index-1])/(self.times[index] - self.times[index-1])
        return self.interpolate(self.values[index-1], self.values[index], fraction)


class TransformKeyFrames:
    """ KeyFrames-like object dedicated to 3D transforms """

    def __init__(self, translate_keys, rotate_keys, scale_keys):
        """ stores 3 keyframe sets for translation, rotation, scale """
        self.translate_kf = KeyFrames(translate_keys)
        self.rotate_kf = KeyFrames(rotate_keys, quaternion_slerp)
        self.scale_kf = KeyFrames(scale_keys)

    def value(self, time):
        """ Compute each component's interpolation and compose TRS matrix """
        T = translate(*self.translate_kf.value(time))
        R = quaternion_matrix(self.rotate_kf.value(time))
        S = scale(*[self.scale_kf.value(time)]*3)
        return T@R@S


class Animation:
    """ Store an animation, which can be applied to a root SkinnedNode """
    def __init__(self, tkf, duration, name):
        self.duration = duration
        # a dictionnary : key = node name, value = TransformKeyFrames
        self.transformed_kf = tkf
        self.name = name


# ------------ low level OpenGL object wrappers ----------------------------
class Shader:
    """ Helper class to create and automatically destroy shader program """
    @staticmethod
    def _compile_shader(src, shader_type):
        src = open(src, 'r').read() if os.path.exists(src) else src
        src = src.decode('ascii') if isinstance(src, bytes) else src
        shader = GL.glCreateShader(shader_type)
        GL.glShaderSource(shader, src)
        GL.glCompileShader(shader)
        status = GL.glGetShaderiv(shader, GL.GL_COMPILE_STATUS)
        src = ('%3d: %s' % (i+1, l) for i, l in enumerate(src.splitlines()))
        if not status:
            log = GL.glGetShaderInfoLog(shader).decode('ascii')
            GL.glDeleteShader(shader)
            src = '\n'.join(src)
            print('Compile failed for %s\n%s\n%s' % (shader_type, log, src))
            sys.exit(1)
        return shader

    def __init__(self, vertex_source, fragment_source, geometry_source=None):
        """ Shader can be initialized with raw strings or source file names """
        self.glid = None
        vert = self._compile_shader(vertex_source, GL.GL_VERTEX_SHADER)
        frag = self._compile_shader(fragment_source, GL.GL_FRAGMENT_SHADER)
        geom = None
        if geometry_source:
            geom = self._compile_shader(geometry_source, GL.GL_GEOMETRY_SHADER)
        if vert and frag:
            self.glid = GL.glCreateProgram()  # pylint: disable=E1111
            GL.glAttachShader(self.glid, vert)
            GL.glAttachShader(self.glid, frag)
            if geom:
                GL.glAttachShader(self.glid, geom)
            GL.glLinkProgram(self.glid)
            GL.glDeleteShader(vert)
            GL.glDeleteShader(frag)
            if geom:
                GL.glDeleteShader(geom)
            status = GL.glGetProgramiv(self.glid, GL.GL_LINK_STATUS)
            if not status:
                print(GL.glGetProgramInfoLog(self.glid).decode('ascii'))
                GL.glDeleteProgram(self.glid)
                sys.exit(1)

    def __del__(self):
        GL.glUseProgram(0)
        if self.glid:                      # if this is a valid shader object
            GL.glDeleteProgram(self.glid)  # object dies => destroy GL object


class VertexArray:
    """ helper class to create and self destroy OpenGL vertex array objects."""

    def __init__(self, attributes, index=None, usage=GL.GL_STATIC_DRAW, divisor=None, primcount=1):
        """ Vertex array from attributes and optional index array. Vertex
            Attributes should be list of arrays with one row per vertex. """

        # create vertex array object, bind it
        self.glid = GL.glGenVertexArrays(1)
        GL.glBindVertexArray(self.glid)
        self.buffers = []  # we will store buffers in a list
        nb_primitives, size = 0, 0

        # load buffer per vertex attribute (in list with index = shader layout)
        loc = 0
        for indice, data in enumerate(attributes):
            if data is not None:
                # bind a new vbo, upload its data to GPU, declare size and type
                self.buffers.append(GL.glGenBuffers(1))
                data = np.array(data, np.float32, copy=False)  # ensure format
                assert(len(data.shape) > 1)
                GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.buffers[-1])
                GL.glBufferData(GL.GL_ARRAY_BUFFER, data, usage)
                
                if nb_primitives == 0:
                    nb_primitives = data.shape[0]
                nloc = 1
                for dim in data.shape[1:-1]:
                    nloc *= dim
                size = data.shape[-1]
                
                sizefloat = 4
                for iloc in range(nloc):
                    GL.glEnableVertexAttribArray(loc)
                    GL.glVertexAttribPointer(loc, size, GL.GL_FLOAT, False, sizefloat*nloc*size, c_void_p(sizefloat*size*iloc))
                    if divisor is not None:
                        GL.glVertexAttribDivisor(loc, divisor[indice])
                    loc += 1

        # optionally create and upload an index buffer for this object
        if index is not None:
            self.buffers += [GL.glGenBuffers(1)]
            index_buffer = np.array(index, np.int32, copy=False)  # good format
            GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, self.buffers[-1])
            GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, index_buffer, usage)
            if divisor is not None:
                self.draw_command = GL.glDrawElementsInstanced
                self.arguments = (index_buffer.size, GL.GL_UNSIGNED_INT, None, primcount)
            else:
                self.draw_command = GL.glDrawElements
                self.arguments = (index_buffer.size, GL.GL_UNSIGNED_INT, None)
        else:
            if divisor is not None:
                self.draw_command = GL.glDrawArraysInstanced
                self.arguments = (0, nb_primitives, primcount)
            else:
                self.draw_command = GL.glDrawArrays
                self.arguments = (0, nb_primitives)

    def execute(self, primitive):
        """ draw a vertex array, either as direct array or indexed array """
        GL.glBindVertexArray(self.glid)
        self.draw_command(primitive, *self.arguments)

    def __del__(self):  # object dies => kill GL array and buffers from GPU
        GL.glDeleteVertexArrays(1, (self.glid,))
        for buffer in self.buffers:
            GL.glDeleteBuffers(1, (buffer, ))


class BoundingBox:
    """ Bounding box a mesh """
    
    def __init__(self, attributes):
        self.mins = np.array([])
        self.maxs = np.array([])
        if attributes is not None and attributes[0] is not None and len(attributes[0]):
            flattened = np.concatenate(attributes[0])
            if len(flattened):
                vertices = np.reshape(flattened, (-1, 3))
                self.mins = np.amin(vertices, 0)
                self.maxs = np.amax(vertices, 0)
        self.center = (self.mins + self.maxs)/2

    def merge(self, other):
        return BoundingBox([[self.mins, other.mins, self.maxs, other.maxs]])

    def transform(self, matrix):
        vertices = []
        for vertice in self.vertices():
            vertice.append(1)
            vertice = matrix @ np.array(vertice)
            vertices.append(vertice[:-1])
        return BoundingBox([vertices])

    def vertices(self):
        if len(self.mins):
            return [
                [self.mins[0], self.mins[1], self.mins[2]],
                [self.mins[0], self.mins[1], self.maxs[2]],
                [self.mins[0], self.maxs[1], self.mins[2]],
                [self.mins[0], self.maxs[1], self.maxs[2]],
                [self.maxs[0], self.mins[1], self.mins[2]],
                [self.maxs[0], self.mins[1], self.maxs[2]],
                [self.maxs[0], self.maxs[1], self.mins[2]],
                [self.maxs[0], self.maxs[1], self.maxs[2]]
            ]
        else:
            return []


# ------------  Mesh is a core drawable, can be basis for most objects --------
class Mesh:
    """ Basic mesh class with attributes passed as constructor arguments """

    def __init__(self, shader, attributes, index=None, transforms=None):
        self.shader = shader
        names = ['view', 'projection', 'model', 'light_dir']
        self.loc = {n: GL.glGetUniformLocation(shader.glid, n) for n in names}
        if transforms is not None:
            divisor = [0] * len(attributes)
            divisor.append(1)
            # to store data just like OpenGL
            for i in range(len(transforms)):
                transforms[i] = np.transpose(transforms[i])
            attributes.append(transforms)
            self.vertex_array = VertexArray(attributes, index, divisor=divisor, primcount=len(transforms))
        else:
            self.vertex_array = VertexArray(attributes, index)
        # first location : vertices -> bounding box
        self.bb = BoundingBox(attributes)
        self.load_info = None

    def draw(self, model, world):
        GL.glUseProgram(self.shader.glid)

        GL.glUniform3fv(self.loc['light_dir'], 1, world.light_dir)
        GL.glUniformMatrix4fv(self.loc['view'], 1, True, world.view)
        GL.glUniformMatrix4fv(self.loc['projection'], 1, True, world.projection)
        GL.glUniformMatrix4fv(self.loc['model'], 1, True, model)

        # draw triangle as GL_TRIANGLE vertex array, draw array call
        self.vertex_array.execute(GL.GL_TRIANGLES)

    def bounding_box(self):
        return self.bb


# -------------- Phong rendered Mesh class -----------------------------------
class PhongMesh(Mesh):
    """ Mesh with Phong illumination """

    def __init__(self, shader, attributes, index=None, transforms=None,
             k_a=(0, 0, 0), k_d=(1, 1, 0), k_s=(1, 1, 1), s=16.):
        super().__init__(shader, attributes, index, transforms)
        self.k_a, self.k_d, self.k_s, self.s = k_a, k_d, k_s, s

        # retrieve OpenGL locations of shader variables at initialization
        names = ['k_a', 's', 'k_s', 'k_d', 'w_camera_position']

        loc = {n: GL.glGetUniformLocation(shader.glid, n) for n in names}
        self.loc.update(loc)

    def draw(self, model, world):
        GL.glUseProgram(self.shader.glid)
        GL.glEnable(GL.GL_STENCIL_TEST)
        GL.glClear(GL.GL_STENCIL_BUFFER_BIT)

        # setup material parameters
        GL.glUniform3fv(self.loc['k_a'], 1, self.k_a)
        GL.glUniform3fv(self.loc['k_d'], 1, self.k_d)
        GL.glUniform3fv(self.loc['k_s'], 1, self.k_s)
        GL.glUniform1f(self.loc['s'], max(self.s, 0.001))

        # world camera position for Phong illumination specular component
        w_camera_position = np.linalg.inv(world.view)[:, 3]
        GL.glUniform3fv(self.loc['w_camera_position'], 1, w_camera_position)

        super().draw(model, world)


# -------------- OpenGL Texture Wrapper ---------------------------------------
class Texture:
    """ Helper class to create and automatically destroy textures """

    def __init__(self, tex_file, wrap_mode=GL.GL_REPEAT, min_filter=GL.GL_LINEAR,
             mag_filter=GL.GL_LINEAR_MIPMAP_LINEAR):
        self.glid = GL.glGenTextures(1)
        try:
            # imports image as a numpy array in exactly right format
            tex = np.asarray(Image.open(tex_file).convert('RGBA'))
            GL.glBindTexture(GL.GL_TEXTURE_2D, self.glid)
            GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, tex.shape[1],
                        tex.shape[0], 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, tex)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, wrap_mode)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, wrap_mode)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, min_filter)
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, mag_filter)
            GL.glGenerateMipmap(GL.GL_TEXTURE_2D)
            message = 'Loaded texture %s\t(%s, %s, %s, %s)'
            print(message % (tex_file, tex.shape, wrap_mode, min_filter, mag_filter))
        except FileNotFoundError:
            print("ERROR: unable to load texture file %s" % tex_file)

    def __del__(self):  # delete GL texture from GPU when object dies
        GL.glDeleteTextures(1, (self.glid,))


class TexturedMesh(PhongMesh):
    """ Simple first textured object """

    def __init__(self, shader, texture, attributes, index=None, **dico):
        super().__init__(shader, attributes, index, **dico)

        loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map')
        self.loc['diffuse_map'] = loc
        # setup texture and upload it to GPU
        self.texture = texture

    def draw(self, model, world):
        GL.glUseProgram(self.shader.glid)

        # texture access setups
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)
        GL.glUniform1i(self.loc['diffuse_map'], 0)

        super().draw(model, world)


class BiTexturedMesh(PhongMesh):
    """ Simple first textured object """

    def __init__(self, shader, texture_1, texture_2, attributes, index=None, **dico):
        super().__init__(shader, attributes, index, **dico)

        loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map_1')
        self.loc['diffuse_map_1'] = loc
        loc = GL.glGetUniformLocation(shader.glid, 'diffuse_map_2')
        self.loc['diffuse_map_2'] = loc
        # setup texture and upload it to GPU
        self.texture_1 = texture_1
        self.texture_2 = texture_2

    def draw(self, model, world):
        GL.glUseProgram(self.shader.glid)

        # texture access setups
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture_1.glid)
        GL.glUniform1i(self.loc['diffuse_map_1'], 0)

        GL.glActiveTexture(GL.GL_TEXTURE1)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture_2.glid)
        GL.glUniform1i(self.loc['diffuse_map_2'], 1)

        super().draw(model, world)


# -------------- Linear Blend Skinning : TP7 ---------------------------------
class SkinnedMesh(TexturedMesh):
    """class of skinned mesh nodes in scene graph """

    def __init__(self, shader, texture, attribs, bone_nodes, bone_offsets, index=None, **kwargs):
        super().__init__(shader, texture, attribs, index, **kwargs)

        # store skinning data
        self.bone_nodes = bone_nodes
        self.bone_offsets = np.array(bone_offsets, np.float32)

    def draw(self, model, world):
        """ skinning object draw method """
        GL.glUseProgram(self.shader.glid)

        # bone world transform matrices need to be passed for skinning
        world_transforms = [node.world_transform for node in self.bone_nodes]
        bone_matrix = world_transforms @ self.bone_offsets
        loc = GL.glGetUniformLocation(self.shader.glid, 'bone_matrix')
        GL.glUniformMatrix4fv(loc, len(self.bone_nodes), True, bone_matrix)

        super().draw(model, world)


# ------------  Node is the core drawable for hierarchical scene graphs -------
class Node:
    """ Scene graph transform and parameter broadcast node """

    def __init__(self, children=(), transform=identity()):
        self.transform = transform
        self.children = list(iter(children))
        self.load_info = None
        self.shape = -1
        self.body = -1
        self.world_scale = [1, 1 ,1]

    def add(self, *drawables):
        """ Add drawables to this node, simply updating children list """
        self.children.extend(drawables)

    def add_shape(self, file, mass=0, scaling=[1, 1, 1], position=[0, 0, 0], orientation=[1, 0, 0, 0], matrix=None, flags=0):
        if matrix is not None:
            position, orientation, scaling = get_TRS_from_matrix(matrix)

        scaling = vec(scaling)
        position = vec(position)
        orientation = vec(orientation)
        self.world_scale = scaling
        self.mass = mass
        
        # Convert w, x, y, z to x, y, z, w for pybullet
        orientation[0:3], orientation[3] = orientation[1:4], orientation[0]
        
        self.shape = pb.createCollisionShape(pb.GEOM_MESH, fileName=file, meshScale=self.world_scale, flags=flags)
        self.body = pb.createMultiBody(self.mass, self.shape, -1, position, orientation)
        print("Forme de collision ajoutée !")

    def draw(self, model, world):
        """ Recursive draw, passing down updated model matrix. """
        if self.body != -1:
            pos, ori = pb.getBasePositionAndOrientation(self.body)
            # convert quaternion from pybullet
            ori = np.array([ori[3], ori[0], ori[1], ori[2]])
            self.world_transform = translate(*pos) @ quaternion_matrix(ori) @ scale(self.world_scale)
            for child in self.children:
                child.draw(self.world_transform, world)
        else:
            self.world_transform = model @ self.transform
            for child in self.children:
                child.draw(self.world_transform, world)
        

    def key_handler(self, key):
        """ Dispatch keyboard events to children """
        for child in self.children:
            if hasattr(child, 'key_handler'):
                child.key_handler(key)

    def bounding_box(self):
        bounding = BoundingBox(None)
        for child in self.children:
            bounding = bounding.merge(child.bounding_box())
        return bounding.transform(self.transform)


class KeyFrameControlNode(Node):
    """ Place node with transform keys above a controlled subtree """

    def __init__(self, translate_keys, rotate_keys, scale_keys):
        super().__init__()
        self.keyframes = TransformKeyFrames(translate_keys, rotate_keys, scale_keys)

    def draw(self, model, world):
        """ When redraw requested, interpolate our node transform from keys """
        self.transform = self.keyframes.value(world.time)
        super().draw(model, world)


# -------- Skinning Control for Keyframing Skinning Mesh Bone Transforms ------
class SkinningControlNode(Node):
    """ Place node with transform keys above a controlled subtree """
    temp_id = 0
    
    def __init__(self, transform=identity()):
        super().__init__(transform=transform)
        self.keyframes = None
        # self.animable_nodes is a dictionnary : key = node name, value = node lookup
        # it is used when self is a root node from an animable model
        self.animable_nodes = None
        self.start_animation = 0
        self.end_animation = 0
        self.world_transform = identity()
        self.animation_speed = 1
        self.shape_file = None
        self.world_scale = [1, 1, 1]
        self.ragdoll_mode = False
        self.center_of_mass = np.array([0, 0, 0])

    def set_animation_speed(self, animation_speed):
        self.animation_speed = animation_speed
        for node_name in self.animable_nodes:
            self.animable_nodes[node_name].animation_speed = self.animation_speed

    def animate(self, anim, time):
        for node_name in anim.transformed_kf:
            node = self.animable_nodes[node_name]
            t_kf = anim.transformed_kf[node_name]
            node.keyframes = t_kf
            node.start_animation = time
            node.end_animation = time + anim.duration / self.animation_speed

    def draw(self, model, world):
        """ When redraw requested, interpolate our node transform from keys """
        if self.keyframes and self.start_animation <= world.time < self.end_animation:
            # No keyframe update should happens if no keyframes
            self.transform = self.keyframes.value((world.time - self.start_animation) * self.animation_speed)

        if self.body != -1:
            if self.ragdoll_mode:
                if self.shape != -1:
                    ####### To use body as ragdoll
                    pb.setCollisionFilterGroupMask(self.body, -1, 1, 1)
                    pos, ori = pb.getBasePositionAndOrientation(self.body)
                    # convert quaternion from pybullet
                    ori = np.array([ori[3], ori[0], ori[1], ori[2]])
                    self.world_transform = translate(*pos) @ quaternion_matrix(ori) @ scale(self.world_scale)
                    self.transform = np.linalg.inv(model) @ self.world_transform
                else:
                    self.world_transform = model @ self.transform
            else:
                ####### To use body as hitbox
                pb.setCollisionFilterGroupMask(self.body, -1, 0, 0)
                self.world_transform = model @ self.transform
                pos, ori, _ = get_TRS_from_matrix(self.world_transform)
                # convert quaternion for pybullet
                ori = np.array([ori[1], ori[2], ori[3], ori[0]])
                pb.resetBasePositionAndOrientation(self.body, pos, ori)
        else:
            # store world transform for skinned meshes using this node as bone
            self.world_transform = model @ self.transform

        # default node behaviour (call children's draw method)
        for child in self.children:
            child.draw(self.world_transform, world)

    def set_ragdoll_mode(self, value):
        self.ragdoll_mode = value
        for child in self.children:
            if isinstance(child, SkinningControlNode):
                child.set_ragdoll_mode(value)

    def hitbox(self, vertices, offset_matrix):
        # equivalent to : (offset_matrix @ vertices^T)^T
        offset_matrix = np.transpose(offset_matrix)
        vertices = np.array(vertices) @ offset_matrix[:3, :3] + offset_matrix[3, :3]
        hull = ConvexHull(vertices, qhull_options="QJ")
        convex_vertices = []
        convert_indice = {}
        for i in hull.vertices:
            convex_vertices.append(vertices[i])
            convert_indice[i] = len(convex_vertices)
        faces = []
        for simplex in hull.simplices:
            faces.append([convert_indice[j] for j in simplex])
        # creating shape from vertices
        tempfile = "temp/temp_%d.obj" % SkinningControlNode.temp_id
        SkinningControlNode.temp_id += 1
        os.makedirs(os.path.dirname(tempfile), exist_ok=True)
        # writing vertices to an obj file
        with open(tempfile, 'w') as fobj:
            for v in convex_vertices:
                fobj.write("v %f %f %f\n" % (v[0], v[1], v[2]))
            for f in faces:
                fobj.write("f %d %d %d\n" % (f[0], f[1], f[2]))
        
        # creating center of mass
        self.center_of_mass = np.mean(convex_vertices, axis=0)
        # loading it to a shape
        self.shape_file = tempfile
    
    def build_body(self, parent_body=-1):
        tr, ro, sc = get_TRS_from_matrix(self.world_transform)
        self.world_scale = sc
        # offseting with CoM
        tr = tr + quaternion_matrix(ro)[:3, :3] @ self.center_of_mass
        # conversion for pybullet
        ro[0:3], ro[3] = ro[1:4], ro[0]
        if self.shape_file is not None:
            self.shape = pb.createCollisionShape(pb.GEOM_MESH, fileName=self.shape_file, collisionFramePosition=-self.center_of_mass, meshScale=self.world_scale)
        self.body = pb.createMultiBody(1, self.shape, -1, tr, ro)
        if parent_body != -1:
            p_pos, p_ori = pb.getBasePositionAndOrientation(parent_body)
            c_pos, c_ori = pb.getBasePositionAndOrientation(self.body)
            p_pos, p_ori, c_pos, c_ori = np.array(p_pos), np.array(p_ori), np.array(c_pos), np.array(c_ori)
            
            p_ori_frame = np.copy(p_ori)
            c_ori_frame = np.copy(c_ori)
            # conversion from pybullet
            p_ori[1:4], p_ori[0] = p_ori[0:3], p_ori[3]
            c_ori[1:4], c_ori[0] = c_ori[0:3], c_ori[3]
            p_mat = quaternion_matrix(p_ori)[:3, :3]
            c_mat = quaternion_matrix(c_ori)[:3, :3]
            
            joint_pos_world = (p_pos + c_pos)/2
            p_pos_frame = np.linalg.inv(p_mat) @ (joint_pos_world-p_pos)
            c_pos_frame = np.linalg.inv(c_mat) @ (joint_pos_world-c_pos)
            
            p_ori_frame[0:3] = -p_ori_frame[0:3]
            c_ori_frame[0:3] = -c_ori_frame[0:3]
            
            self.constraint = pb.createConstraint(parent_body, -1, self.body, -1, pb.JOINT_FIXED, [0, 0, 1], p_pos_frame, c_pos_frame, p_ori_frame, c_ori_frame)
            pb.changeConstraint(self.constraint, maxForce=50000)
            # no collision with parent
            pb.setCollisionFilterPair(parent_body, self.body, -1, -1, 0)

        for child in self.children:
            if isinstance(child, SkinningControlNode):
                child.build_body(self.body)


    def remove_body(self):
        if self.body != -1:
            pb.removeBody(self.body)
            self.body = -1
        for child in self.children:
            if isinstance(child, SkinningControlNode):
                child.remove_body()


# ------------  Viewer class & window management ------------------------------
class Viewer(Node):
    """ GLFW viewer window, with classic initialization & graphics loop """

    def __init__(self, width=640, height=480, light_dir=(0, 0, 1)):
        super().__init__()

        self.light_dir = light_dir

        # version hints: create GL window with >= OpenGL 3.3 and core profile
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL.GL_TRUE)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        glfw.window_hint(glfw.RESIZABLE, True)
        self.win = glfw.create_window(width, height, 'Viewer', None, None)

        # make win's OpenGL context current; no OpenGL calls can happen before
        glfw.make_context_current(self.win)

        # initialize trackball
        self.trackball = Trackball()
        self.mouse = (0, 0)

        # register event handlers
        glfw.set_key_callback(self.win, self.on_key)
        glfw.set_cursor_pos_callback(self.win, self.on_mouse_move)
        glfw.set_scroll_callback(self.win, self.on_scroll)
        glfw.set_window_size_callback(self.win, self.on_size)

        # useful message to check OpenGL renderer characteristics
        print('OpenGL', GL.glGetString(GL.GL_VERSION).decode() + ', GLSL',
              GL.glGetString(GL.GL_SHADING_LANGUAGE_VERSION).decode() + ', Renderer',
              GL.glGetString(GL.GL_RENDERER).decode())

        # initialize GL by setting viewport and default render characteristics
        GL.glClearColor(0.1, 0.1, 0.1, 0.1)
        GL.glEnable(GL.GL_CULL_FACE)   # backface culling enabled (TP2)
        GL.glEnable(GL.GL_DEPTH_TEST)  # depth test now enabled (TP2)

        # cyclic iterator to easily toggle polygon rendering modes
        self.fill_modes = cycle([GL.GL_LINE, GL.GL_POINT, GL.GL_FILL])

    def run(self):
        """ Main render loop for this OpenGL window """
        nframe = 0
        debut = glfw.get_time()
        while not glfw.window_should_close(self.win):
            nframe += 1
            # clear draw buffer and depth buffer (<-TP2)
            GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

            win_size = glfw.get_window_size(self.win)
            self.view = self.trackball.view_matrix()
            self.projection = self.trackball.projection_matrix(win_size)

            # draw our scene objects
            self.draw(identity(), self)

            # flush render commands, and swap draw buffers
            glfw.swap_buffers(self.win)

            # Poll for and process events
            glfw.poll_events()
        fin = glfw.get_time()
        fps = nframe/(fin - debut)
        print("Moyenne : {} fps".format(fps))

    def on_key(self, _win, key, _scancode, action, _mods):
        """ 'Q' or 'Escape' quits """
        if action == glfw.PRESS or action == glfw.REPEAT:
            if key == glfw.KEY_ESCAPE or key == glfw.KEY_Q:
                glfw.set_window_should_close(self.win, True)
            if key == glfw.KEY_W:
                GL.glPolygonMode(GL.GL_FRONT_AND_BACK, next(self.fill_modes))

            # call Node.key_handler which calls key_handlers for all drawables
            self.key_handler(key)

    def on_mouse_move(self, win, xpos, ypos):
        """ Rotate on left-click & drag, pan on right-click & drag """
        old = self.mouse
        self.mouse = (xpos, glfw.get_window_size(win)[1] - ypos)
        if glfw.get_mouse_button(win, glfw.MOUSE_BUTTON_LEFT):
            self.trackball.drag(old, self.mouse, glfw.get_window_size(win))
        if glfw.get_mouse_button(win, glfw.MOUSE_BUTTON_RIGHT):
            self.trackball.pan(old, self.mouse)

    def on_scroll(self, win, _deltax, deltay):
        """ Scroll controls the camera distance to trackball center """
        self.trackball.zoom(deltay, glfw.get_window_size(win)[1])

    def on_size(self, win, _width, _height):
        """ window size update => update viewport to new framebuffer size """
        GL.glViewport(0, 0, *glfw.get_framebuffer_size(win))
