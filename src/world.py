from core import Node
from transform import identity, vec, translate, rotate, norm2
import glfw
import OpenGL.GL as GL
from itertools import cycle
import numpy as np
import math
import pybullet as pb
from threading import Thread
from time import sleep
from blood import BloodSource


def normalized(vec):
    return vec/np.linalg.norm(vec)


def engine_steper(win):
    tick = 0.01
    pb.setTimeStep(tick)
    while not glfw.window_should_close(win):
        begin = glfw.get_time()
        pb.stepSimulation()
        end = glfw.get_time()
        if (tick > end - begin):
            sleep(tick - end + begin)


class KeyListener:
    """ Listen to the given keys to retrieve press time """

    def __init__(self):
        self.press_time = 0
        self.pressed = False
        self.last_press = 0

    def update(self, action):
        if (action == glfw.PRESS or action == glfw.REPEAT) and not self.pressed:
            self.pressed = True
            self.last_press = glfw.get_time()
        elif action == glfw.RELEASE and self.pressed:
            self.pressed = False
            self.press_time += glfw.get_time() - self.last_press

    def pop_press_time(self):
        pop_time = glfw.get_time()
        if self.pressed:
            self.press_time += pop_time - self.last_press
            self.last_press = pop_time
        to_return = self.press_time
        self.press_time = 0
        return to_return


class World(Node):
    """ GLFW viewer window, with classic initialization & graphics loop """

    def __init__(self, width=1600, height=900, light_dir=(0, 0, 1)):
        super().__init__()
        # current time of simulation
        self.time = 0
        self.delta_time = 0
        
        self.fly_mode = False

        # for camera
        self.position = vec((1200, 200, 0))
        self.jump_count = 0
        radius = 15
        sphere = pb.createCollisionShape(pb.GEOM_SPHERE, radius=radius)
        self.body_offset = vec(0, 1.5*radius, 0)
        self.upper_body = pb.createMultiBody(1, sphere, -1, self.position + self.body_offset, [0, 0, 0, 1])
        self.lower_body = pb.createMultiBody(1, sphere, -1, self.position - self.body_offset, [0, 0, 0, 1])
        pb.changeDynamics(self.lower_body, -1, lateralFriction=1, linearDamping=0, angularDamping=1)
        pb.createConstraint(
            parentBodyUniqueId=self.upper_body,
            parentLinkIndex=-1,
            childBodyUniqueId=self.lower_body,
            childLinkIndex=-1,
            jointType=pb.JOINT_FIXED,
            jointAxis=[1, 0, 0],
            parentFramePosition=[0, 0, 0],
            childFramePosition=[0, 0, 0, 1]
        )
        
        self.angles = vec((0, 0, 0))

        # one light for the whole world
        self.light_dir = light_dir

        # sensibility to controlls
        self.sensi_mouse = 0.1
        self.sensi_keyboard = 500

        # to throw things
        self.flip = False
        self.object_collision_file = None
        self.object_visual_root = None

        # default position of the cursor
        self.xpos_default = int(width/2)
        self.ypos_default = int(height/2)
        self.mouse = (self.xpos_default, self.ypos_default)

        self.hud = None

        # controll keys
        self.forward_key = glfw.KEY_W
        self.backward_key = glfw.KEY_S
        self.right_key = glfw.KEY_D
        self.left_key = glfw.KEY_A
        self.up_key = glfw.KEY_SPACE
        self.down_key = glfw.KEY_LEFT_SHIFT
        used_keys = (self.forward_key, self.backward_key, self.left_key, self.right_key, self.up_key, self.down_key)

        self.keys = {key: KeyListener() for key in used_keys}
        
        # screen parameters
        self.fovx = 90
        self.znear = 0.01
        self.zfar = 10000

        # version hints: create GL window with >= OpenGL 3.3 and core profile
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL.GL_TRUE)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        glfw.window_hint(glfw.RESIZABLE, True)
        self.win = glfw.create_window(width, height, 'Medieval 3D - The Game', None, None)

        # make win's OpenGL context current; no OpenGL calls can happen before
        glfw.make_context_current(self.win)

        # register event handlers
        glfw.set_input_mode(self.win, glfw.CURSOR, glfw.CURSOR_HIDDEN)
        glfw.set_key_callback(self.win, self.on_key)
        glfw.set_cursor_pos_callback(self.win, self.on_mouse_move)
        glfw.set_scroll_callback(self.win, self.on_scroll)
        glfw.set_window_size_callback(self.win, self.on_size)
        glfw.set_mouse_button_callback(self.win, self.on_mouse_click)

        # useful message to check OpenGL renderer characteristics
        print('OpenGL', GL.glGetString(GL.GL_VERSION).decode() + ', GLSL',
              GL.glGetString(GL.GL_SHADING_LANGUAGE_VERSION).decode() + ', Renderer',
              GL.glGetString(GL.GL_RENDERER).decode())

        # initialize GL by setting viewport and default render characteristics
        GL.glClearColor(0.1, 0.1, 0.1, 0.1)
        GL.glEnable(GL.GL_CULL_FACE)   # backface culling enabled (TP2)
        GL.glEnable(GL.GL_DEPTH_TEST)  # depth test now enabled (TP2)
        # Pour la transparence
        # GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
        # GL.glEnable(GL.GL_BLEND)

        # cyclic iterator to easily toggle polygon rendering modes
        self.fill_modes = cycle([GL.GL_LINE, GL.GL_POINT, GL.GL_FILL])

        # A list a functions executed every frame
        # (the functions must take no parameters)
        self.routines = []

        self.entities = []
        self.blood_shader = None

    def add_routine(self, routine):
        self.routines.append(routine)

    def update_keys(self):
        """ Update position and angles depending on key pressed """
        rad = math.radians(self.angles[1])
        si = math.sin(rad)
        co = math.cos(rad)
        forward = vec(-si, 0, co)
        right = vec(co, 0, si)
        up = vec(0, 1, 0)

        forward_coeff = -self.keys[self.forward_key].pop_press_time() + self.keys[self.backward_key].pop_press_time()
        up_coeff = self.keys[self.up_key].pop_press_time() - self.keys[self.down_key].pop_press_time()
        right_coeff = self.keys[self.right_key].pop_press_time() - self.keys[self.left_key].pop_press_time()

        if self.fly_mode:
            self.position = self.position + self.sensi_keyboard*(forward_coeff*forward + up_coeff*up + right_coeff*right)
        else:
            lower_pos = pb.getBasePositionAndOrientation(self.lower_body)[0]
            self.position = vec(*lower_pos) + self.body_offset
            if self.jump_count <= 0 and up_coeff > 0:
                for contact in pb.getContactPoints(bodyA=self.lower_body):
                    if contact[7][1] > 0.5:
                        self.jump_count = 0.5
                        continue
            if self.jump_count > 0:
                self.jump_count -= up_coeff
            else:
                up_coeff = 0
            if forward_coeff != 0 or right_coeff != 0 or up_coeff != 0:
                force = 1000*self.sensi_keyboard*(forward*forward_coeff + right*right_coeff + up*up_coeff)
                pb.applyExternalForce(self.lower_body, -1, force, [0, 0, 0], pb.WORLD_FRAME)

    def update_matrices(self):
        """ Update view and projection matrices """
        self.update_keys()

        winsize = glfw.get_window_size(self.win)
        _scale = 1.0/math.tan(math.radians(self.fovx)/2.0)
        sx, sy = _scale, _scale * winsize[0] / winsize[1]
        zz = (self.zfar + self.znear) / (self.znear - self.zfar)
        zw = 2 * self.zfar * self.znear / (self.znear - self.zfar)
        self.projection = np.array([[sx, 0, 0, 0],
                                    [0, sy, 0, 0],
                                    [0, 0, zz, zw],
                                    [0, 0, -1, 0]], 'f')
        self.view = \
            rotate((0., 0., 1.), self.angles[2]) @ \
            rotate((1., 0., 0.), self.angles[0]) @ \
            rotate((0., 1., 0.), self.angles[1]) @ \
            translate(-self.position)

    def run(self):
        """ Main render loop for this OpenGL window """
        nframe = 0
        debut = 0
        engine_thread = Thread(target=engine_steper, args=(self.win, ))
        engine_thread.start()
        while not glfw.window_should_close(self.win):
            if nframe > 20:
                print("{} fps".format(nframe/(glfw.get_time() - debut)))
                nframe = 0
                debut = glfw.get_time()
            nframe += 1
            # clear depth buffer (<-TP2)
            GL.glClear(GL.GL_DEPTH_BUFFER_BIT)
            GL.glClear(GL.GL_COLOR_BUFFER_BIT)

            # draw our scene objects
            self.update_matrices()
            new_time = glfw.get_time()
            self.delta_time = new_time - self.time
            self.time = new_time
            self.draw(identity(), self)
            if self.hud is not None:
                self.hud.draw()

            # flush render commands, and swap draw buffers
            glfw.swap_buffers(self.win)

            # Poll for and process events
            glfw.poll_events()

            for routine in self.routines:
                routine()
        engine_thread.join()

    def on_key(self, _win, key, _scancode, action, _mods):
        """ 'Q' or 'Escape' quits """
        if key in self.keys:
            self.keys[key].update(action)

        if action == glfw.PRESS or action == glfw.REPEAT:
            # if key == glfw.KEY_ESCAPE or key == glfw.KEY_Q:
            #     glfw.set_window_should_close(self.win, True)
            # if key == glfw.KEY_W:
            #     GL.glPolygonMode(GL.GL_FRONT_AND_BACK, next(self.fill_modes))

            if key == glfw.KEY_TAB:
                self.fly_mode = not self.fly_mode

            # call Node.key_handler which calls key_handlers for all drawables
            self.key_handler(key)

    def on_mouse_move(self, win, xpos, ypos):
        """ Update camera angles """
        self.angles[1] += self.sensi_mouse*(xpos - self.xpos_default)
        self.angles[0] += self.sensi_mouse*(ypos - self.ypos_default)

        if self.angles[0] > 90:
            self.angles[0] = 90

        if self.angles[0] < -90:
            self.angles[0] = -90

        glfw.set_cursor_pos(win, self.xpos_default, self.ypos_default)

    def throw_object(self):
        """throw an object from player's position"""
        mat = np.linalg.inv(self.view)
        front = mat[:3, :3] @ vec(0, 0, -100)
        # mat = mat @ translate(front[0], front[1], front[2])
        mat = translate(self.position + front)
        node = Node()
        node.add(self.object_visual_root)
        node.add_shape(self.object_collision_file, mass=1, matrix=mat)
        pb.changeDynamics(node.body, -1, linearDamping=0, angularDamping=0)
        speed = 1000 * front
        pb.resetBaseVelocity(node.body, speed, [0, 0, 0])
        self.add(node)

    def on_mouse_click(self, win, button, action, mods):
        if action == glfw.PRESS:
            if button == glfw.MOUSE_BUTTON_LEFT:
                if self.object_visual_root is not None and self.object_collision_file is not None:
                    self.throw_object()
            if button == glfw.MOUSE_BUTTON_RIGHT:
                for entity in self.entities:
                    if norm2(self.position - entity.position) < 10000 and not entity.is_dead:
                        entity.kill(self.time)
                        if self.blood_shader:
                            self.add(BloodSource(self.blood_shader, entity.position + vec(0, 50, 0),
                                                 20, (0, 0, 0), 0, 30, 100))

    def on_scroll(self, win, _deltax, deltay):
        """ Nothing to do yet """
        pass

    def on_size(self, win, _width, _height):
        """ window size update => update viewport to new framebuffer size """
        GL.glViewport(0, 0, *glfw.get_framebuffer_size(win))

    def add_entity(self, entity):
        self.entities.append(entity)
        self.add(entity)
