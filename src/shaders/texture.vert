#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv_coordinate;
layout(location = 2) in vec3 normal;

out vec2 frag_tex_coords;
out vec3 w_position, w_normal;

void main() {
    vec4 w_4position = model * vec4(position, 1.0);
    
    gl_Position = projection * view * w_4position;
    w_position = w_4position.xyz;
    w_normal = transpose(inverse(mat3(model))) * normal;

    frag_tex_coords = uv_coordinate;
}
