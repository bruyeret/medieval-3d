#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float interpolation_ratio;
uniform float translation_ratio;

layout(location = 0) in vec3 position_1;
layout(location = 1) in vec3 position_2;
layout(location = 2) in vec2 uv_coordinate;
layout(location = 3) in vec3 normal_1;
layout(location = 4) in vec3 normal_2;

out VS_OUT {
    vec2 frag_tex_coords;
    vec3 w_position, w_normal;
    float wrap;
} vs_out;

float sigmoid(float x) {
    return x * x * (3 - 2 * x);
}

void main() {
    vs_out.wrap = 0;
    vec3 position;
    vec3 normal;
    float interpolation_ratio_corrected = 0;
    if (interpolation_ratio <= 1 && interpolation_ratio >= 0) {
        interpolation_ratio_corrected = sigmoid(interpolation_ratio);
    }
    else if (interpolation_ratio <= 2) {
        interpolation_ratio_corrected = sigmoid(2 - interpolation_ratio);
    }
    position = interpolation_ratio_corrected * position_1 + (1 - interpolation_ratio_corrected) * position_2;
    normal = interpolation_ratio_corrected * normal_1 + (1 - interpolation_ratio_corrected) * normal_2;
    position.x += 2 * translation_ratio;
    if (position.x > 1) {
        vs_out.wrap = 1;
        position.x -= 2;
    }
    vec4 w_4position = model * vec4(position, 1.0);

    gl_Position = projection * view * w_4position;
    vs_out.w_position = w_4position.xyz;
    vs_out.w_normal = transpose(inverse(mat3(model))) * normal;

    vs_out.frag_tex_coords = uv_coordinate;
}
