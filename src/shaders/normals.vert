#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout(location = 0) in vec3 position;
layout(location = 2) in vec3 normal;

out vec3 w_normal;

void main() {
    vec4 w_4position = model * vec4(position, 1.0);
    gl_Position = projection * view * w_4position;
    w_normal = transpose(inverse(mat3(model))) * normal;
}
