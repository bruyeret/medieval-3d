#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

in VS_OUT {
    vec2 frag_tex_coords;
    vec3 w_position, w_normal;
    float wrap;
} gs_in[];

out vec2 frag_tex_coords;
out vec3 w_position, w_normal;

void main() {
    float wrap = gs_in[0].wrap + gs_in[1].wrap + gs_in[2].wrap;
    if (wrap == 1 || wrap == 2) {
        for (int i = 0; i < 3; i++) {
            vec3 position = (inverse(model) * vec4(gs_in[i].w_position, 1)).xyz;
            wrap = gs_in[i].wrap;
            if (wrap == 1) {
                position.x += 2;
                wrap = 0;
            }
            vec4 w_4position = model * vec4(position, 1.0);
            gl_Position = projection * view * w_4position;
            frag_tex_coords = gs_in[i].frag_tex_coords;
            w_position = w_4position.xyz;
            w_normal = gs_in[i].w_normal;
            EmitVertex();
        }
    }
    EndPrimitive();

    for (int i = 0; i < 3; i++) {
        gl_Position = gl_in[i].gl_Position;
        frag_tex_coords = gs_in[i].frag_tex_coords;
        w_position = gs_in[i].w_position;
        w_normal = gs_in[i].w_normal;
        wrap = gs_in[i].wrap;
        EmitVertex();
    }
    EndPrimitive();
}  