#version 330 core
out vec4 FragColor;
uniform sampler2D u_textgrass;

in GS_OUT {vec2 textCoord;} fs_in;

void main(){
    vec4 color = texture(u_textgrass, fs_in.textCoord);
    if (color.a < 0.05) discard;
    FragColor = color;
}
