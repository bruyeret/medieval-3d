#version 330 core

// world camera position
uniform vec3 w_camera_position;
in vec3 w_position, w_normal;   // in world coodinates

// light dir, in world coordinates
uniform vec3 light_dir;

// material properties
uniform vec3 k_d;
uniform vec3 k_a;
uniform vec3 k_s;
uniform float s;

// output fragment color for OpenGL
out vec4 out_color;

void main() {
    vec3 n = normalize(w_normal);
    vec3 v = normalize(w_camera_position - w_position);
    vec3 r = reflect(-light_dir, n);
    out_color = vec4(k_a + k_s * pow(max(0, dot(r, v)), s) + k_d * max(0, dot(n, light_dir)), 1);
}
