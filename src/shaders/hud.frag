#version 330 core

uniform sampler2D diffuse_map;

in vec2 uv;
out vec4 out_color;

void main() {
    out_color = texture(diffuse_map, vec2(uv.x, 1-uv.y));
    if (out_color[3] < 0.5)
        discard;
}
