#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv_coordinate;
layout(location = 2) in vec3 normal;

out vec2 frag_tex_coords;

void main() {
    gl_Position = projection * vec4(mat3(view) * position, 1.0);
    frag_tex_coords = uv_coordinate;
}
