#version 330 core

uniform sampler2D diffuse_map;
out vec4 out_color;

// world camera position
uniform vec3 w_camera_position;
in vec3 w_normal;   // in world coodinates

// light dir, in world coordinates
uniform vec3 light_dir;

// material properties
uniform vec3 k_d;
uniform vec3 k_a;
uniform vec3 k_s;
uniform float s;

void main() {
    vec3 n = normalize(w_normal);
    out_color = vec4((n + vec3(1, 1, 1)) / 2, 1);
}
