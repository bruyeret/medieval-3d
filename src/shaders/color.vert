#version 330 core

// input attribute variable, given per vertex
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

// global matrix variables
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 w_position;
out vec3 w_normal;

void main() {
    vec4 w_4position = model * vec4(position, 1.0);
    
    gl_Position = projection * view * w_4position;
    w_position = w_4position.xyz;
    w_normal = transpose(inverse(mat3(model))) * normal;
}
