#version 330 core

out vec4 out_color;

// light dir, in world coordinates
uniform vec3 light_dir;
uniform mat4 projection, view;

in vec2 uv_coord;
in vec4 v_position, v_centre;

const float radius = 10;

void main() {
    float dist_rayon = length(uv_coord);
    if (dist_rayon >= 1) {
        discard;
    } else {
        float z_offset = radius*sqrt(1-dist_rayon*dist_rayon);
        vec4 new_v_position = v_position + vec4(0, 0, z_offset, 0);
        gl_FragDepth = ((projection[3].z / new_v_position.z) + projection[2].z - 1.0) / -2.0;
        
        vec3 v_light = -mat3(view)*light_dir;
        vec3 n = normalize(vec3(new_v_position - v_centre));
        vec3 v = normalize(vec3(-new_v_position));
        vec3 r = reflect(-v_light, n);
        
        const vec3 k_a = vec3(0.2, 0, 0);
        const vec3 k_d = vec3(0.2, 0, 0);
        const vec3 k_s = vec3(0.3, 0.3, 0.3);
        const float s = 10;
        out_color = vec4(k_a + k_s * pow(max(0, dot(r, v)), s) + k_d * max(0, dot(n, v_light)), 1);
    }
}
