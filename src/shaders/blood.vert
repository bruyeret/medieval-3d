#version 330 core

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 position;

uniform mat4 view, projection;

out vec2 uv_coord;
out vec4 v_position, v_centre;

const float radius = 10;

void main() {
    v_centre = view * vec4(position, 1.0);
    v_position = v_centre + radius*vec4(vertex, 0);
    gl_Position = projection * v_position;
    uv_coord = vec2(vertex);
}
