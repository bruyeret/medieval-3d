#version 330 core

// ---- camera geometry
uniform mat4 projection, view;

// ---- skinning globals and attributes
const int MAX_VERTEX_BONES=4, MAX_BONES=128;
uniform mat4 bone_matrix[MAX_BONES];

// ---- vertex attributes
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv_coordinate;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec4 bone_ids;
layout(location = 4) in vec4 bone_weights;

// ----- interpolated attribute variables to be passed to fragment shader
out vec2 frag_tex_coords;
out vec3 w_position, w_normal;

void main() {

    // ------ creation of the skinning deformation matrix
    mat4 skin_matrix = mat4(0.);
    for (int i = 0; i < MAX_VERTEX_BONES; ++i)
        skin_matrix += bone_matrix[int(bone_ids[i])]*bone_weights[i];

    // ------ compute world and normalized eye coordinates of our vertex
    vec4 w_position4 = skin_matrix * vec4(position, 1.0);
    gl_Position = projection * view * w_position4;

    w_position = w_position4.xyz;
    w_normal = transpose(inverse(mat3(skin_matrix))) * normal;
    
    frag_tex_coords = uv_coordinate;
}
