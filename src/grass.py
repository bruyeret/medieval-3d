import numpy as np
from transform import vec
from core import VertexArray
import OpenGL.GL as GL
class Medow:
    def __init__(self, shader, texture, height_function):
        self.positions = []
        for i in range(21):
            for j in range(21):
                x = -1 + i/10
                z = -1 + j/10
                y = height_function(x, z)
                self.positions.append(vec(x, y, z))
        #self.vertices = np.reshape(self.positions, (-1, 3))
        self.vertices = []
        self.faces = []
        self.texture = texture
        self.shader = shader
        self.glid = GL.glGenVertexArrays(1)
        GL.glBindVertexArray(self.glid)           
        self.buffers = [GL.glGenBuffers(1)]      

        GL.glEnableVertexAttribArray(0)         
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.buffers[0])
        GL.glBufferData(GL.GL_ARRAY_BUFFER, np.array(self.positions), GL.GL_STATIC_DRAW)   
        GL.glVertexAttribPointer(0, 3, GL.GL_FLOAT, False, 0, None)      
    
    def draw(self, model, world):
        GL.glUseProgram(self.shader.glid)
        #GL.glUniform3fv(self.loc['light_dir'], 1, world.light_dir)
        GL.glUniformMatrix4fv(GL.glGetUniformLocation(self.shader.glid, 'u_view'), 1, True, world.view)
        GL.glUniformMatrix4fv(GL.glGetUniformLocation(self.shader.glid, 'u_projection'), 1, True, world.projection)
        GL.glUniformMatrix4fv(GL.glGetUniformLocation(self.shader.glid, 'u_model'), 1, True, model)

        GL.glUniform1i(GL.glGetUniformLocation(self.shader.glid,\
                "u_textgrass"), 0)
        GL.glActiveTexture(GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture.glid)


    def __del__(self):  # object dies => kill GL array and buffers from GPU
        GL.glDeleteVertexArrays(1, (self.glid,))
        for buffer in self.buffers:
            GL.glDeleteBuffers(1, (buffer, ))

