#!/usr/bin/env python3
"""
Python OpenGL practical application.
"""
# Python built-in modules
import os

# External, non built-in modules
import assimpcy                     # 3D resource loader
import numpy as np
from transform import identity
import pybullet as pb

from core import Texture, TexturedMesh, PhongMesh, SkinnedMesh, SkinningControlNode,\
    Animation, TransformKeyFrames, Node

MAX_VERTEX_BONES = 4
MAX_BONES = 128

# -------------- 3D resource loader -----------------------------------------
def load_phong_mesh(file, shader):
    """ load resources from file using assimp, return list of ColorMesh """
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_GenSmoothNormals
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []

    # prepare mesh nodes
    meshes = []
    for mesh in scene.mMeshes:
        mat = scene.mMaterials[mesh.mMaterialIndex].properties
        attributes = [mesh.mVertices, mesh.mNormals]
        mesh = PhongMesh(shader, attributes, mesh.mFaces,
                         k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                         k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                         k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                         s=mat.get('SHININESS', 16.))
        meshes.append(mesh)

    size = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(meshes), size))
    return meshes


def load_textured(file, shader, tex_file=None, tex_dict=None):
    """ load resources from file using assimp, return list of TexturedMesh """
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_FlipUVs
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []

    # Note: embedded textures not supported at the moment
    path = os.path.dirname(file) if os.path.dirname(file) != '' else './'
    for mat in scene.mMaterials:
        if not tex_file and 'TEXTURE_BASE' in mat.properties:  # texture token
            name = os.path.basename(mat.properties['TEXTURE_BASE'])
            # search texture in file's whole subdir since path often screwed up
            paths = os.walk(path, followlinks=True)
            found = [os.path.join(d, f) for d, _, n in paths for f in n
                     if name.startswith(f) or f.startswith(name)]
            assert found, 'Cannot find texture %s in %s subtree' % (name, path)
            tex_file = found[0]
        if tex_file:
            if tex_dict:
                if tex_file in tex_dict:
                    mat.properties['diffuse_map'] = tex_dict[tex_file]
                else:
                    tex = Texture(tex_file=tex_file)
                    mat.properties['diffuse_map'] = tex
                    tex_dict[tex_file] = tex
            else:
                mat.properties['diffuse_map'] = Texture(tex_file=tex_file)

    # prepare textured mesh
    meshes = []
    for mesh in scene.mMeshes:
        mat = scene.mMaterials[mesh.mMaterialIndex].properties
        assert mat['diffuse_map'], "Trying to map using a textureless material"
        attributes = [mesh.mVertices, mesh.mTextureCoords[0], mesh.mNormals]
        mesh = TexturedMesh(shader, mat['diffuse_map'], attributes, mesh.mFaces,
                            k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                            k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                            k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                            s=mat.get('SHININESS', 16.))

        meshes.append(mesh)

    size = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded %s\t(%d meshes, %d faces)' % (file, len(meshes), size))
    return meshes


def conv(assimp_keys, ticks_per_second):
    """ Conversion from assimp key struct to our dict representation """
    return {key.mTime / ticks_per_second: key.mValue for key in assimp_keys}


def load_animation(file):
    """load the keyframes from a file, return KeyFrames"""
    try:
        scene = assimpcy.aiImportFile(file, 0)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []
    animations = []
    # load first animation in scene file (could be a loop over all animations)
    chan = 0
    for anim in scene.mAnimations:
        transform_keyframes = {}
        for channel in anim.mChannels:
            chan += 1
            # for each animation bone, store TRS dict with {times: transforms}
            transform_keyframes[channel.mNodeName] = TransformKeyFrames(
                conv(channel.mPositionKeys, anim.mTicksPerSecond),
                conv(channel.mRotationKeys, anim.mTicksPerSecond),
                conv(channel.mScalingKeys, anim.mTicksPerSecond)
            )
        # an animation has been loaded and can be added to a Node
        duration = anim.mDuration/anim.mTicksPerSecond
        name = anim.mName
        if name[0:2] == "b'" and name [-1] == "'":
            name = name[2:-1]
        animations.append(Animation(transform_keyframes, duration, name))
        print('Loaded', file, '\t(%d animations, %d channels)' % (scene.mNumAnimations, chan))
    return animations


def load_model(file, shader_anim, shader_tex, shader_color, tex_file=None, tex_dict=None, save_vertices=True):
    """load resources from file using assimp, return node hierarchy """
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_GenSmoothNormals | pp.aiProcess_FlipUVs
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []

    # Note: embedded textures not supported at the moment
    path = os.path.dirname(file) if os.path.dirname(file) != '' else './'
    for mat in scene.mMaterials:
        if not tex_file and 'TEXTURE_BASE' in mat.properties:  # texture token
            name = os.path.basename(mat.properties['TEXTURE_BASE'])
            # search texture in file's whole subdir since path often screwed up
            paths = os.walk(path, followlinks=True)
            found = []
            for d, _, n in paths:
                for f in n:
                    if name.endswith(f):
                        found.append(os.path.join(d, f))
            assert found, 'Cannot find texture %s in %s subtree' % (name, path)
            tex_file = found[0]
        if tex_file:
            if tex_dict is not None:
                if tex_file in tex_dict:
                    mat.properties['diffuse_map'] = tex_dict[tex_file]
                else:
                    tex = Texture(tex_file=tex_file)
                    mat.properties['diffuse_map'] = tex
                    tex_dict[tex_file] = tex
            else:
                mat.properties['diffuse_map'] = Texture(tex_file=tex_file)

    # ---- prepare scene graph nodes
    # create SkinningControlNode for each assimp node.
    # node creation needs to happen first as SkinnedMeshes store an array of
    # these nodes that represent their bone transforms
    nodes = {}                                       # nodes name -> node lookup
    nodes_per_mesh_id = [[] for _ in scene.mMeshes]  # nodes holding a mesh_id

    def make_nodes(assimp_node):
        """ Recursively builds nodes for our graph, matching assimp nodes """
        skin_node = SkinningControlNode(transform=assimp_node.mTransformation)
        nodes[assimp_node.mName] = skin_node
        for mesh_index in assimp_node.mMeshes:
            nodes_per_mesh_id[mesh_index].append(skin_node)
        skin_node.add(*(make_nodes(child) for child in assimp_node.mChildren))
        return skin_node

    root_node = make_nodes(scene.mRootNode)

    # ---- create SkinnedMesh objects
    for mesh_id, assimp_mesh in enumerate(scene.mMeshes):
        
        mat = scene.mMaterials[assimp_mesh.mMaterialIndex].properties
        if ('diffuse_map' not in mat):
            # Load mesh as Phong mesh with no texture
            attributes = [assimp_mesh.mVertices, assimp_mesh.mNormals]
            mesh = PhongMesh(shader_color, attributes, assimp_mesh.mFaces,
                             k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                             k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                             k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                             s=mat.get('SHININESS', 16.))
        elif (not assimp_mesh.mBones):
            # Load mesh as a simple textured mesh
            attributes = [assimp_mesh.mVertices, assimp_mesh.mTextureCoords[0], assimp_mesh.mNormals]
            mesh = TexturedMesh(shader_tex, mat['diffuse_map'], attributes, assimp_mesh.mFaces,
                                k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                                k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                                k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                                s=mat.get('SHININESS', 16.))
        else:
            # Load mesh as a skinned and textured mesh
            # first, populate an array with MAX_BONES entries per vertex
            v_bone = np.array([[(0, 0)]*MAX_BONES] * assimp_mesh.mNumVertices,
                              dtype=[('weight', 'f4'), ('id', 'u4')])
            for bone_id, bone in enumerate(assimp_mesh.mBones[:MAX_BONES]):
                for entry in bone.mWeights:  # weight,id pairs necessary for sorting
                    v_bone[entry.mVertexId][bone_id] = (entry.mWeight, bone_id)

            v_bone.sort(order='weight')             # sort rows, high weights last
            v_bone = v_bone[:, -MAX_VERTEX_BONES:]  # limit bone size, keep highest

            # prepare bone lookup array & offset matrix, indexed by bone index (id)
            bone_nodes = [nodes[bone.mName] for bone in assimp_mesh.mBones]
            bone_offsets = [bone.mOffsetMatrix for bone in assimp_mesh.mBones]

            # for each node, get corresponding "most responsible" vertices
            vertices_per_bone_id = {}
            for v_id, v in enumerate(assimp_mesh.mVertices):
                bone_id = v_bone[v_id][-1]['id']
                if bone_id not in vertices_per_bone_id:
                    vertices_per_bone_id[bone_id] = []
                vertices_per_bone_id[bone_id].append(v)
                
                if v_bone[v_id][-2]['weight'] > 0.2:
                    bone_id = v_bone[v_id][-2]['id']
                    if bone_id not in vertices_per_bone_id:
                        vertices_per_bone_id[bone_id] = []
                    vertices_per_bone_id[bone_id].append(v)
            
            # creating hitbox shape for each bone
            for bone_id in vertices_per_bone_id:
                if len(vertices_per_bone_id[bone_id]) >= 8:
                    assimp_bone = assimp_mesh.mBones[bone_id]
                    node = nodes[assimp_bone.mName]
                    node.hitbox(vertices_per_bone_id[bone_id], assimp_bone.mOffsetMatrix)

            # initialize skinned mesh and store in assimp mesh for node addition
            attrib = [assimp_mesh.mVertices, assimp_mesh.mTextureCoords[0], assimp_mesh.mNormals, v_bone['id'], v_bone['weight']]
            mesh = SkinnedMesh(shader_anim, mat['diffuse_map'], attrib, bone_nodes, bone_offsets,
                               assimp_mesh.mFaces,
                               k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                               k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                               k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                               s=mat.get('SHININESS', 16.))

        if save_vertices:
            mesh.vertices = assimp_mesh.mVertices
            mesh.faces = assimp_mesh.mFaces

        # Add the mesh on the corresponding nodes
        for node in nodes_per_mesh_id[mesh_id]:
            node.add(mesh)

    nb_triangles = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded', file, '\t(%d meshes, %d faces, %d nodes)' %
          (scene.mNumMeshes, nb_triangles, len(nodes)))
    root_node.animable_nodes = nodes
    root_node.load_info = (file, shader_anim, shader_tex, shader_color, tex_file, tex_dict)
    return [root_node]


def load_model_instance(file, shader_tex_instance, shader_color_instance, matrices, tex_file=None, tex_dict=None, save_vertices=True, parent_transform=identity()):
    """load resources from file using assimp, return node hierarchy """
    try:
        pp = assimpcy.aiPostProcessSteps
        flags = pp.aiProcess_Triangulate | pp.aiProcess_GenSmoothNormals | pp.aiProcess_FlipUVs
        scene = assimpcy.aiImportFile(file, flags)
    except assimpcy.all.AssimpError as exception:
        print('ERROR loading', file + ': ', exception.args[0].decode())
        return []

    # Note: embedded textures not supported at the moment
    path = os.path.dirname(file) if os.path.dirname(file) != '' else './'
    for mat in scene.mMaterials:
        if not tex_file and 'TEXTURE_BASE' in mat.properties:  # texture token
            name = os.path.basename(mat.properties['TEXTURE_BASE'])
            # search texture in file's whole subdir since path often screwed up
            paths = os.walk(path, followlinks=True)
            found = []
            for d, _, n in paths:
                for f in n:
                    if name.endswith(f):
                        found.append(os.path.join(d, f))
            assert found, 'Cannot find texture %s in %s subtree' % (name, path)
            tex_file = found[0]
        if tex_file:
            if tex_dict is not None:
                if tex_file in tex_dict:
                    mat.properties['diffuse_map'] = tex_dict[tex_file]
                else:
                    tex = Texture(tex_file=tex_file)
                    mat.properties['diffuse_map'] = tex
                    tex_dict[tex_file] = tex
            else:
                mat.properties['diffuse_map'] = Texture(tex_file=tex_file)

    def make_meshes(assimp_node, node_transform=identity()):
        """ Recursively builds nodes for our graph, matching assimp nodes """
        meshes = []
        node_transform = node_transform @ assimp_node.mTransformation
        for mesh_id in assimp_node.mMeshes:
            assimp_mesh = scene.mMeshes[mesh_id]
            mat = scene.mMaterials[assimp_mesh.mMaterialIndex].properties
            
            if ('diffuse_map' not in mat):
                # Load mesh as Phong mesh with no texture
                attributes = [assimp_mesh.mVertices, assimp_mesh.mNormals]
                mesh = PhongMesh(shader_color_instance, attributes, assimp_mesh.mFaces,
                                 transforms=parent_transform @ np.array(matrices) @ node_transform,
                                 k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                                 k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                                 k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                                 s=mat.get('SHININESS', 16.))
            else:
                # Load mesh as a simple textured mesh
                attributes = [assimp_mesh.mVertices, assimp_mesh.mTextureCoords[0], assimp_mesh.mNormals]
                mesh = TexturedMesh(shader_tex_instance, mat['diffuse_map'], attributes, assimp_mesh.mFaces,
                                    transforms=parent_transform @ np.array(matrices) @ node_transform,
                                    k_d=mat.get('COLOR_DIFFUSE', (1, 1, 1)),
                                    k_s=mat.get('COLOR_SPECULAR', (1, 1, 1)),
                                    k_a=mat.get('COLOR_AMBIENT', (0, 0, 0)),
                                    s=mat.get('SHININESS', 16.))

            if save_vertices:
                mesh.vertices = assimp_mesh.mVertices
                mesh.faces = assimp_mesh.mFaces
            
            meshes.append(mesh)

        for child in assimp_node.mChildren:
            meshes = meshes + make_meshes(child, node_transform)

        return meshes

    meshes = make_meshes(scene.mRootNode)

    nb_triangles = sum((mesh.mNumFaces for mesh in scene.mMeshes))
    print('Loaded', file, '\t(%d meshes, %d faces, %d times)' %
          (scene.mNumMeshes, nb_triangles, len(matrices)))
    return meshes



def load_scene(filename, shader_anim, shader_tex, shader_color, shader_tex_instance, shader_color_instance, tex_file=None, tex_dict=None, mat_parent=identity()):
    lines_per_model = 17
    nodes = []
    with open(filename, 'r') as file:
        lines = file.readlines()
    mat_dict = dict()
    for i in range(0, len(lines), lines_per_model):
        node_file = lines[i][:-1]
        mat = np.zeros((4, 4))
        for j in range(4):
            for k in range(4):
                mat[j][k] = float(lines[i+1+k+4*j][:-1])
        if (node_file not in mat_dict):
            mat_dict[node_file] = []
        mat_dict[node_file].append(mat)
    
    for file in mat_dict:
        mats = mat_dict[file]
        node = Node()
        children = []
        if len(mats) == 1:
            children = load_model(file, shader_anim, shader_tex, shader_color, tex_file=None, tex_dict=None)
            node.transform = mats[0]
        else:
            children = load_model_instance(file, shader_tex_instance, shader_color_instance, mats, tex_file=None, tex_dict=None, parent_transform=mat_parent)
        node.add(*children)
        nodes.append(node)
        
        file_collision = os.path.splitext(file)[0] + ".obj"
        if os.path.isfile(file_collision):
            for mat in mats:
                node.add_shape(file_collision, matrix=mat_parent @ mat, flags=pb.GEOM_FORCE_CONCAVE_TRIMESH)
    
    return nodes
