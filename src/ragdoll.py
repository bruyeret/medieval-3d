import pybullet as pb
import glfw
from transform import identity, vec, scale
from core import Shader, Node
from loader import load_model
from world import World
from entity import Archer


def main():
    glfw.init()
    pb.connect(pb.DIRECT)
    pb.resetDebugVisualizerCamera(cameraDistance=200, cameraYaw=0, cameraPitch=-20, cameraTargetPosition=[0, 0, 0])
    plane_shape = pb.createCollisionShape(pb.GEOM_PLANE, planeNormal=[0, 1, 0])
    pb.createMultiBody(0, plane_shape, -1, [0, -50, 0])
    
    world = World()
    
    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")
    
    file = "../models/pack1/archer/archer_walking.FBX"
    # file = "../models/pack1/armored_swordman/armored_swordman_dying.FBX"
    # file = "../models/pack1/swordman/swordman_dying.FBX"
    control_node = load_model(file, shader_anim, shader_tex, shader_color)[0]
    control_node.build_body(identity())
    world.add(control_node)
    
    pb.setGravity(0, -500, 0)
    world.run()
    
    pb.disconnect()
    glfw.terminate()

def main2():
    glfw.init()
    pb.connect(pb.DIRECT)
    pb.resetDebugVisualizerCamera(cameraDistance=50, cameraYaw=0, cameraPitch=-80, cameraTargetPosition=[0, -20, 0])
    plane_shape = pb.createCollisionShape(pb.GEOM_PLANE, planeNormal=[0, 1, 0])
    pb.createMultiBody(0, plane_shape, -1, [0, -50, 0])
    
    world = World()
    
    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")
    
    node_archer = Node()
    node_archer = Node()
    archer = Archer(shader_anim, shader_tex, shader_color)
    archer.position = vec(-100, 30, 0)
    node_archer.add(archer)
    world.add(node_archer)
    
    pb.setGravity(0, -500, 0)
    world.run()
    
    pb.disconnect()
    glfw.terminate()


if __name__ == "__main__":
    main2()