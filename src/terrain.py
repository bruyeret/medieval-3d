#!/usr/bin/env python3

import numpy as np                  # all matrix manipulations & OpenGL args
from core import TexturedMesh, Texture, BiTexturedMesh, Node
from transform import lerp, vec, normalized, smooth_interpolation, identity, translate
import OpenGL.GL as GL
import glfw


class Terrain(TexturedMesh):
    """
    Generates some terrain based on a height function evaluated
    in [-1, 1]x[-1, 1] at some sample points.
    """
    def __init__(self, shader, texture, number_of_points, height_function):

        self.height_function = height_function

        positions = Terrain.get_positions(number_of_points, height_function)
        uv_map = Terrain.get_uv_map(number_of_points)
        normals = Terrain.get_normals(number_of_points, positions)
        index = Terrain.get_indexes(number_of_points)

        super().__init__(shader, texture, [positions, uv_map, normals], index, k_a=(1, 1, 1))
        
        self.vertices = np.reshape(positions, (-1, 3))
        self.faces = np.reshape(index, (-1, 3))

    @staticmethod
    def get_positions(number_of_points, height_function):
        positions = []
        for j in range(number_of_points):
            z = -1 + 2 * j / (number_of_points - 1)
            for i in range(number_of_points):
                x = -1 + 2 * i / (number_of_points - 1)
                positions.append(np.array([x, height_function(x, z), z]))
        return np.array(positions, 'f')

    @staticmethod
    def get_uv_map(number_of_points):
        uv_map = []
        for i in range(number_of_points):
            u = i / (number_of_points - 1)
            for j in range(number_of_points):
                v = j / (number_of_points - 1)
                uv_map.append((10 * u, 10 * v))
        return np.array(uv_map, 'f')

    @staticmethod
    def get_normals(number_of_points, positions):
        normals = []
        for j in range(number_of_points):
            for i in range(number_of_points):
                if i == 0:
                    xm1 = positions[number_of_points - 1 + j * number_of_points] - vec(2, 0, 0)
                    xp1 = positions[i + 1 + j * number_of_points]
                elif i == number_of_points - 1:
                    xm1 = positions[i - 1 + j * number_of_points]
                    xp1 = positions[j * number_of_points] + vec(2, 0, 0)
                else:
                    xm1 = positions[i - 1 + j * number_of_points]
                    xp1 = positions[i + 1 + j * number_of_points]

                if j == 0:
                    zm1 = positions[i + (number_of_points - 1) * number_of_points] - vec(0, 0, 2)
                    zp1 = positions[i + (j + 1) * number_of_points]
                elif j == number_of_points - 1:
                    zm1 = positions[i + (j - 1) * number_of_points]
                    zp1 = positions[i] + vec(0, 0, 2)
                else:
                    zm1 = positions[i + (j - 1) * number_of_points]
                    zp1 = positions[i + (j + 1) * number_of_points]

                normal = normalized(np.cross(xp1 - xm1, zp1 - zm1))
                if normal[1] < 0:
                    normal = -normal
                normals.append(normal)
        return np.array(normals, 'f')

    @staticmethod
    def get_indexes(number_of_points):
        index = []
        for i in range(number_of_points - 1):
            for j in range(number_of_points - 1):
                index.append(i + j * number_of_points)
                index.append(i + (j + 1) * number_of_points)
                index.append(i + 1 + (j + 1) * number_of_points)
                index.append(i + j * number_of_points)
                index.append(i + 1 + (j + 1) * number_of_points)
                index.append(i + 1 + j * number_of_points)
        return np.array(index, np.uint32)


class TerrainTransition(BiTexturedMesh):

    @staticmethod
    def get_texture_ratios(number_of_points, tex_function):
        texture_ratios = []
        for j in range(number_of_points):
            z = -1 + 2 * j / (number_of_points - 1)
            for i in range(number_of_points):
                x = -1 + 2 * i / (number_of_points - 1)
                texture_ratios.append(tex_function(x, z))
        return np.array(texture_ratios, 'f').reshape(-1, 1)

    def __init__(self, shader, texture_1, texture_2, number_of_points, height_function, tex_function):
        positions = Terrain.get_positions(number_of_points, height_function)
        uv_map = Terrain.get_uv_map(number_of_points)
        normals = Terrain.get_normals(number_of_points, positions)
        texture_ratios = TerrainTransition.get_texture_ratios(number_of_points, tex_function)
        index = Terrain.get_indexes(number_of_points)

        super().__init__(shader, texture_1, texture_2, [positions, uv_map, normals, texture_ratios], index, k_a=(1, 1, 1))
        
        self.vertices = np.reshape(positions, (-1, 3))
        self.faces = np.reshape(index, (-1, 3))


class Water(TexturedMesh):
    def __init__(self, shader, texture, number_of_points, wave_period=5, translation_period=20):
        def get_index(i, j):
            return i + j * number_of_points

        self.wave_period = wave_period
        self.translation_period = translation_period

        p_low_1 = Perlin2D(5)
        p_high_1 = Perlin2D(10)
        p_low_2 = Perlin2D(5)
        p_high_2 = Perlin2D(10)

        height_function_1 = lambda x, z: p_low_1.evaluate(x, z) + .5 * p_high_1.evaluate(x, z)
        height_function_2 = lambda x, z: p_low_2.evaluate(x, z) + .5 * p_high_2.evaluate(x, z)

        position_1 = Terrain.get_positions(number_of_points, height_function_1)
        position_2 = Terrain.get_positions(number_of_points, height_function_2)
        uv_map = Terrain.get_uv_map(number_of_points)
        normal_1 = Terrain.get_normals(number_of_points, position_1)
        normal_2 = Terrain.get_normals(number_of_points, position_2)
        index = Terrain.get_indexes(number_of_points)

        super().__init__(shader, texture, [position_1, position_2, uv_map, normal_1, normal_2], index, k_a=(1, 1, 1))
        self.loc["interpolation_ratio"] = GL.glGetUniformLocation(shader.glid, "interpolation_ratio")
        self.loc["translation_ratio"] = GL.glGetUniformLocation(shader.glid, "translation_ratio")
        
        self.vertices = []
        self.faces = []

    def draw(self, model, world):
        GL.glUseProgram(self.shader.glid)
        interpolation_ratio = 2 * (glfw.get_time() % self.wave_period) / self.wave_period
        translation_ratio = (glfw.get_time() % self.translation_period) / self.translation_period
        GL.glUniform1f(self.loc["interpolation_ratio"], interpolation_ratio)
        GL.glUniform1f(self.loc["translation_ratio"], translation_ratio)
        super().draw(model, world)


class Perlin2D:
    def __init__(self, number_of_points, interpolation_func=lerp, seed=None):
        if seed is not None:
            np.random.seed(seed)
        self.number_of_points = number_of_points
        self.grid = np.zeros((number_of_points, number_of_points, 2))
        self.interpolation_func = interpolation_func
        for i in range(number_of_points):
            for j in range(number_of_points):
                theta = 2 * np.pi * np.random.rand()
                self.grid[i, j] = (np.cos(theta), np.sin(theta))

    def compute_scalar(self, i, j, x, y):
        ii = i
        jj = j
        if i == self.number_of_points - 1:
            ii = 0
        if j == self.number_of_points - 1:
            jj = 0
        return (x - i) * self.grid[ii, jj][0] + (y - j) * self.grid[ii, jj][1]

    def evaluate(self, x, y):
        # transforming x and y into [-1, 1]
        x = ((x + 1) % 2) - 1
        y = ((y + 1) % 2) - 1
        # transforming x and y into [0, number_of_points - 1]
        x = (x + 1) / 2 * (self.number_of_points - 1)
        y = (y + 1) / 2 * (self.number_of_points - 1)

        i = int(x)
        j = int(y)

        tmp1 = self.compute_scalar(i, j, x, y)
        tmp2 = self.compute_scalar(i + 1, j, x, y)
        tmp3 = self.interpolation_func(tmp1, tmp2, x - i)
        tmp1 = self.compute_scalar(i, j + 1, x, y)
        tmp2 = self.compute_scalar(i + 1, j + 1, x, y)
        tmp1 = self.interpolation_func(tmp1, tmp2, x - i)
        return self.interpolation_func(tmp3, tmp1, y - j)


class ChunkGrid(Node):
    """
    Contains chunks inside a grid of 2x2 squares
    chunk (0, 0) is at [-1, 1]x[-1, 1]
    """
    def __init__(self, transform=identity(), render_distance=1):
        super().__init__(transform=transform)
        self.chunks = dict()
        self.render_distance = render_distance

    def add_to_chunk(self, item, x, z, y=0):
        if (x, z) not in self.chunks:
            self.chunks[(x, z)] = Node(transform=translate(2 * x, 0, 2 * z))
        if y != 0:
            node = Node(transform=translate(0, y, 0))
            node.add(item)
            item = node
        self.chunks[(x, z)].add(item)

    """
    Adds an item to the chunk grid, whose position is
    specified in the parent node of the chunk grid.
    """
    def add_to_chunkgrid(self, item):
        node = Node(transform=np.linalg.inv(self.transform))
        node.add(item)
        center = node.bounding_box().center()
        x = int(np.floor((center[0] + 1) / 2))
        z = int(np.floor((center[2] + 1) / 2))
        self.add_to_chunk(node, x, z)

    def draw(self, model, world):
        # super().draw(model, world)
        model = model @ self.transform
        x, y, z = world.position
        position = np.linalg.inv(model) @ vec(x, y, z, 1)
        x_chunk = int(np.floor((position[0] + 1) / 2))
        z_chunk = int(np.floor((position[2] + 1) / 2))
        for x in range(x_chunk - self.render_distance, x_chunk + self.render_distance + 1):
            for z in range(z_chunk - self.render_distance, z_chunk + self.render_distance + 1):
                if (x, z) in self.chunks:
                    self.chunks[(x, z)].draw(model, world)


if __name__ == "__main__":
    import pybullet as pb
    from world import World
    from transform import scale
    from core import Shader, Node
    from terrain_functions import descending_slope, convert, smoothstep, global_topology

    glfw.init()                # initialize window system glfw

    pb.connect(pb.DIRECT)

    world = World()
    world.fly_mode = True

    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")
    shader_normals = Shader("shaders/normals.vert", "shaders/normals.frag")
    shader_water = Shader("shaders/water.vert", "shaders/water.frag", "shaders/water.geom")
    shader_tex_trans = Shader("shaders/texture_transition.vert", "shaders/texture_transition.frag")

    grass_texture = Texture("../textures/grass.png")
    sand_texture = Texture("../textures/sand.png")
    water_texture = Texture("../textures/water.png")
    snow_texture = Texture("../textures/snow.jpg")

    # On charge la scène

    p = Perlin2D(5, interpolation_func=smooth_interpolation, seed=0)
    chunk_grid = ChunkGrid(transform=scale(500, 100, 500), render_distance=1)
    terrain = Terrain(shader_tex, grass_texture, 100, p.evaluate)
    for i in range(-3, 1):
        for j in range(-3, 4):
            if (i, j) != (-1, 0):
                chunk_grid.add_to_chunk(terrain, i, j)
    water = Water(shader_water, water_texture, 100)
    water_node = Node(transform=scale(1, -.5, 1))
    water_node_2 = Node(transform=scale(1, -.5, 1))
    water_node.add(water)
    water_node_2.add(water)
    chunk_grid.add_to_chunk(water_node, 2, 0, -.5)
    chunk_grid.add_to_chunk(water_node_2, 3, 0, -.5)
    beach = TerrainTransition(shader_tex_trans, grass_texture, sand_texture, 100,
                              lambda x, z: descending_slope(convert(x)) * p.evaluate(x, z),
                              lambda x, z: (x + 1) / 2)
    chunk_grid.add_to_chunk(beach, 1, 0)
    ocean_transition = Terrain(shader_tex, sand_texture, 100,
                               lambda x, z: descending_slope(1 - convert(x)) * (p.evaluate(x, z) - 2 * smoothstep(convert(x))))
    chunk_grid.add_to_chunk(ocean_transition, 2, 0)
    ocean = Terrain(shader_tex, sand_texture, 100, p.evaluate)
    chunk_grid.add_to_chunk(ocean, 3, 0, -2)

    mountain = TerrainTransition(shader_tex_trans, snow_texture, grass_texture, 100,
                                 lambda x, z: global_topology(p.evaluate, x, z),
                                 lambda x, z: (x ** 2 + z ** 2) / 2 + .5)
    chunk_grid.add_to_chunk(mountain, -1, 0)
    world.add(chunk_grid)

    # On lance la simulation
    world.run()

    glfw.terminate()           # destroy all glfw windows and GL contexts
