#!/usr/bin/env python3
import glfw
from transform import vec
import OpenGL.GL as GL
import numpy as np


class BloodSource:

    def __init__(self, shader, position, particles_per_second, normal, speed=50, fuzzy=10, max_particles=1000):
        self.shader = shader
        self.position = vec(position)
        self.normal = vec(normal)
        self.speed = speed
        self.fuzzy = fuzzy
        self.pps = particles_per_second
        self.maxp = max_particles
        self.particles = np.zeros((self.maxp, 3), dtype=np.float32)
        self.speeds = np.zeros((self.maxp, 3), dtype=np.float32)
        self.last_time = -1

        self.added_particles = 0  # number of particles added
        self.next_particle = 0  # indice of next particle to add
        self.add_remainder = 0

        names = ['view', 'projection', 'light_dir']
        self.loc = {n: GL.glGetUniformLocation(shader.glid, n) for n in names}

        # generating VAO
        self.vao = GL.glGenVertexArrays(1)
        GL.glBindVertexArray(self.vao)

        # generating the vertex buffer
        self.vert_vbo = GL.glGenBuffers(1)
        GL.glEnableVertexAttribArray(0)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.vert_vbo)
        vertex = np.array(
            [[-1, -1, 0],
             [ 1, -1, 0],
             [-1,  1, 0],
             [ 1,  1, 0]],
            dtype=np.float32
        )
        GL.glBufferData(GL.GL_ARRAY_BUFFER, vertex, GL.GL_STATIC_DRAW)
        GL.glVertexAttribPointer(0, 3, GL.GL_FLOAT, False, 0, None)
        GL.glVertexAttribDivisor(0, 0)

        # generating the position buffer
        self.pos_vbo = GL.glGenBuffers(1)
        GL.glEnableVertexAttribArray(1)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.pos_vbo)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self.maxp, None, GL.GL_STREAM_DRAW)
        GL.glVertexAttribPointer(1, 3, GL.GL_FLOAT, False, 0, None)
        GL.glVertexAttribDivisor(1, 1)


    def draw(self, model, world):
        # updating particles
        if self.last_time != -1:
            delta_t = world.time - self.last_time
            # adding some particles
            self.add_remainder += delta_t*self.pps
            to_add = int(self.add_remainder)
            self.add_remainder -= to_add
            for i in range(to_add):
                self.particles[self.next_particle] = self.position
                self.speeds[self.next_particle] = self.speed*self.normal + 2*self.fuzzy*(np.random.rand(3)-0.5)

                if self.added_particles < self.maxp:
                    self.added_particles += 1
                self.next_particle = (self.next_particle + 1)%self.maxp
            # updating speeds and positions
            for i in range(self.added_particles):
                self.particles[i] = self.particles[i] + delta_t * self.speeds[i]
                # if world.terrain is not None:
                #     self.particles[i] = world.terrain.collide(world.transform, self.particles[i])
                self.speeds[i] = self.speeds[i] + delta_t*vec(0, -100, 0)
        self.last_time = world.time

        # drawing particles
        GL.glUseProgram(self.shader.glid)

        GL.glUniform3fv(self.loc['light_dir'], 1, world.light_dir)
        GL.glUniformMatrix4fv(self.loc['view'], 1, True, world.view)
        GL.glUniformMatrix4fv(self.loc['projection'], 1, True, world.projection)

        # updating the pos vbo
        GL.glBindVertexArray(self.vao)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self.pos_vbo)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self.particles, GL.GL_STREAM_DRAW)

        GL.glDrawArraysInstanced(GL.GL_TRIANGLE_STRIP, 0, 4, self.added_particles)

    def collide(self, model, new_pos, speed):
        return new_pos

class BloodCell:

    def __init__(self, position=(0, 0, 0), radius=10):
        self.position = vec(position)
        self.radius = radius

    def distance(self, offset, axis):
        return self.position[axis] - offset


def main():
    glfw.init()
    world = World()
    shader_blood = Shader("shaders/blood.vert", "shaders/blood.frag")
    bs = BloodSource(shader_blood, (0, 0, 0), 10, (0, 1, 0), 10, 3, 1000)
    world.add(bs)
    world.run()
    glfw.terminate()


if __name__ == '__main__':
    from world import World
    from core import Shader
    main()
