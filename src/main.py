#!/usr/bin/env python3

import glfw                         # lean window system wrapper for OpenGL
from loader import load_scene, load_textured, load_model
from core import Shader, Node, Texture
from world import World
from transform import scale, smooth_interpolation, translate, vec, normalized
from terrain import Terrain, TerrainTransition, Water, Perlin2D, ChunkGrid
from terrain_functions import descending_slope, smoothstep, convert, global_topology
from entity import Archer, Swordman
import pybullet as pb
from vhacd_script import write_obj
from hud import HUD


def main():
    pb.connect(pb.DIRECT)

    pb.setRealTimeSimulation(0)
    pb.setGravity(0, -500, 0)

    world = World(light_dir=normalized(vec(0, 1, 1)))

    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_tex_instance = Shader("shaders/texture_instance.vert", "shaders/texture.frag")
    shader_tex_trans = Shader("shaders/texture_transition.vert", "shaders/texture_transition.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")
    shader_color_instance = Shader("shaders/color_instance.vert", "shaders/color.frag")
    shader_skybox = Shader("shaders/skybox.vert", "shaders/skybox.frag")
    shader_water = Shader("shaders/water.vert", "shaders/water.frag", "shaders/water.geom")

    scene_path = "scene_2.txt"
    # Dictionnaire utile pour ne pas charger plusieurs fois la même texture
    tex_dict = dict()

    # On charge la scène
    node = Node()
    node.children = load_scene(scene_path, shader_anim, shader_tex, shader_color, shader_tex_instance, shader_color_instance, tex_dict=tex_dict, mat_parent=node.transform)
    world.add(node)

    world.add(load_textured("../models/skybox/skybox.obj", shader_skybox)[0])

    # Ajout du terrain
    grass_texture = Texture("../textures/grass.png")
    sand_texture = Texture("../textures/sand.png")
    water_texture = Texture("../textures/water.png")
    snow_texture = Texture("../textures/snow.jpg")

    print("Generating Perlin noise")
    p = Perlin2D(5, interpolation_func=smooth_interpolation, seed=0)
    chunk_grid = ChunkGrid(transform=scale(500, 100, 500))
    print("Creating terrain mesh")
    terrain = Terrain(shader_tex, grass_texture, 100, p.evaluate)
    for i in range(-2, 1):
        for j in range(-2, 3):
            if (i, j) != (-1, 2):
                chunk_grid.add_to_chunk(terrain, i, j)
    mountain = TerrainTransition(shader_tex_trans, snow_texture, grass_texture, 100,
                                 lambda x, z: global_topology(p.evaluate, x, z),
                                 lambda x, z: (x ** 2 + z ** 2) / 2 + .5)
    chunk_grid.add_to_chunk(mountain, -1, 2)
    print("Creating water mesh")
    water = Water(shader_water, water_texture, 100)
    water_node = Node(transform=scale(1, -.5, 1))
    water_node.add(water)
    print("Creating beach mesh")
    beach = TerrainTransition(shader_tex_trans, grass_texture, sand_texture, 100,
                              lambda x, z: descending_slope(convert(x)) * p.evaluate(x, z),
                              lambda x, z: (x + 1) / 2)
    print("Creating submarine mesh")
    ocean_transition = Terrain(shader_tex, sand_texture, 100, lambda x, z: descending_slope(1 - convert(x)) * (
                p.evaluate(x, z) - 2 * smoothstep(convert(x))))
    ocean = Terrain(shader_tex, sand_texture, 100, p.evaluate)
    for i in range(-1, 2):
        chunk_grid.add_to_chunk(beach, 1, i)
        chunk_grid.add_to_chunk(ocean_transition, 2, i)
        chunk_grid.add_to_chunk(ocean, 3, i, -2)
        chunk_grid.add_to_chunk(water_node, 2, i, -.5)
        chunk_grid.add_to_chunk(water_node, 3, i, -.5)
    world.add(chunk_grid)

    # collisions pour le terrain
    terrain_file = "terrain.obj"
    with open(terrain_file, 'w') as fout:
        write_obj(chunk_grid, fout)
    chunk_grid.add_shape(terrain_file, flags=pb.GEOM_FORCE_CONCAVE_TRIMESH)

    archer = Archer(shader_anim, shader_tex, shader_color)
    world.add_entity(archer)

    archer.routine_start = glfw.get_time()
    archer.waypoint_index = 0
    archer.waypoints = [vec(-500, 0, 0), vec(0, 0, 500)]

    def archer_routine():
        if archer.target is None and glfw.get_time() - archer.routine_start > 5:
            archer.set_target(archer.waypoints[archer.waypoint_index])
            archer.waypoint_index = (archer.waypoint_index + 1) % len(archer.waypoints)
            archer.routine_start = glfw.get_time()
    world.add_routine(archer_routine)

    swordman = Swordman(shader_anim, shader_tex, shader_color)
    swordman.position = vec(100, 0, 0)
    world.add_entity(swordman)

    shader_blood = Shader("shaders/blood.vert", "shaders/blood.frag")
    world.blood_shader = shader_blood

    cube_file = "../models/cube.obj"
    cube_model = load_model(cube_file, shader_anim, shader_tex, shader_color)[0]
    # Pour pouvoir lancer des trucs
    world.object_visual_root = cube_model
    world.object_collision_file = cube_file

    world.position = vec(320, 50, 50)
    node = Node()
    node.add(cube_model)
    node.add_shape(cube_file, mass=0, matrix=translate(345, 25, 50) @ scale(2, 1, 10))
    world.add(node)
    for i in range(2):
        for j in range(7):
            node = Node()
            node.add(cube_model)
            node.add_shape(cube_file, mass=0.1, matrix=translate(345, 55+40.1*i, 20.01*j + 3) @ scale(1, 2, 1))
            world.add(node)
    # Gestion du HUD
    shader_hud = Shader("shaders/hud.vert", "shaders/hud.frag")
    hud = HUD(shader_hud, Texture("../textures/hud.png"))
    hud.add_quad((-1, -1, 1, -1, -1, 1, 1, 1), (0, 0, 1, 0, 0, 1, 1, 1))
    # hud.add_quad((-1, -1, 1, -1, -1, 1, 1, 1), (0, 0, 400/1920, 0, 0, 300/1080, 400/1920, 300/1080))
    # hud.add_quad((-0.5, -1, 0.5, -1, -0.5, -0.8, 0.5, -0.8), (400/1920, 0, (400+364)/1920, 0, 400/1920, 44/1080, (400+364)/1920, 44/1080))
    world.hud = hud

    # On lance la simulation
    world.run()
    pb.disconnect()


if __name__ == "__main__":
    glfw.init()                # initialize window system glfw
    main()                     # main function keeps variables locally scoped
    glfw.terminate()           # destroy all glfw windows and GL contexts
