#!/usr/bin/env python3
from world import World
import glfw
from loader import load_model, load_textured, load_scene
import os
from core import Node, Shader, Texture
from transform import translate, rotate, scale, smooth_interpolation
from terrain import Terrain, TerrainTransition, Water, Perlin2D, ChunkGrid
from terrain_functions import descending_slope, convert, smoothstep
import pybullet as pb
import sys


class Creator(World):
    """ Import, duplicate, move, rotate, delete elements with this creator """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # load all models from the models folder
        self.fly_mode = True
        self.node_rotate = 0
        self.node_scale = 1
        self.scroll_sensi = 10
        self.scrolled = 0
        self.loadable_nodes = []
        self.loadable_access = []
        self.tex_dict = dict()
        # node to display models before importing them
        self.front_node = Node()
        self.add(self.front_node)
        # indice of the current selected Node in loadable_nodes or None if nothing selected
        self.selected_to_import = 0
        # modes :
        # 0 = just moving, nothing selected
        # 1 = selecting something to import
        # 2 = some scene object has been selected
        self.mode = 0

    def load(self, path, shader_anim, shader_tex, shader_color):
        paths = os.walk(path, followlinks=True)
        n_before = len(self.loadable_nodes)
        for d, _, n in paths:
            for f in n:
                if f.endswith(".fbx") or f.endswith(".FBX"):
                    file = os.path.join(d, f)
                    try:
                        loaded = load_model(file, shader_anim, shader_tex, shader_color, tex_dict=self.tex_dict)
                        for indice, mesh in enumerate(loaded):
                            self.loadable_nodes.append(mesh)
                            self.loadable_access.append((file, indice))
                    except:
                        print("Failed to load model : ", file)
        n_after = len(self.loadable_nodes)
        print("Loaded a total of %s models : went from %s to %s." % (n_after - n_before, n_before, n_after))

    def run(self):
        super().run()
        self.save_scene()

    def save_scene(self):
        filename = "scene.txt"
        with open(filename, 'w') as file:
            for node in self.children:
                if node is not self.front_node and node.load_info is not None:
                    file.write(node.load_info[0])
                    file.write("\n")
                    for i in range(4):
                        for j in range(4):
                            file.write(str(node.transform[i][j]))
                            file.write("\n")

    def draw(self, model, world):
        # updating front Node matrix
        self.front_node_update()
        super().draw(model, world)

    def switch_mode(self, n):
        if n == 0:
            self.front_node.children = []
            self.mode = 0
        elif n == 1:
            self.front_node_update()
            self.mode = 1
        elif n == 2:
            self.mode = 2

    def front_node_update(self):
        node = self.loadable_nodes[self.selected_to_import]
        self.front_node.children = [node]
        bb = self.front_node.children[0].bounding_box()
        self.front_node.transform = \
            translate(self.position) @ \
            rotate((0., 1., 0.), -self.angles[1]) @ \
            rotate((1., 0., 0.), -self.angles[0]) @ \
            translate(-(bb.mins + bb.maxs)/2) @ \
            translate(z=self.scrolled*self.scroll_sensi) @ \
            scale(self.node_scale, self.node_scale, self.node_scale) @\
            rotate((1., 0., 0.), self.node_rotate + self.angles[0])

    def on_key(self, _win, key, _scancode, action, _mods):
        if (action == glfw.PRESS):
            # use left and right arrow to select an imported model
            if (key == glfw.KEY_LEFT):
                self.scrolled = 0
                self.selected_to_import = (self.selected_to_import - 1)%len(self.loadable_nodes)
                self.switch_mode(1)
            if (key == glfw.KEY_RIGHT):
                self.scrolled = 0
                self.selected_to_import = (self.selected_to_import + 1)%len(self.loadable_nodes)
                self.switch_mode(1)
            
            if (key == glfw.KEY_UP):
                self.node_rotate += 90
            if (key == glfw.KEY_DOWN):
                self.node_rotate -= 90
            
            if (key == glfw.KEY_KP_SUBTRACT):
                self.node_scale *= 0.7
            if (key == glfw.KEY_KP_ADD):
                self.node_scale *= 1/0.7

            # use escape to cancel any action
            if (key == glfw.KEY_ESCAPE):
                self.switch_mode(0)
            
            
        super().on_key(_win, key, _scancode, action, _mods)

    def on_scroll(self, win, _deltax, deltay):
        self.scrolled += deltay
        super().on_scroll(win, _deltax, deltay)

    def on_mouse_click(self, win, button, action, mods):
        if (button == glfw.MOUSE_BUTTON_LEFT and action == glfw.PRESS):
            if self.mode == 1:
                front_node_child = self.front_node.children[0]
                assert front_node_child.load_info is not None
                node = load_model(*front_node_child.load_info)[0]
                node.load_info = front_node_child.load_info
                node.transform = self.front_node.transform @ node.transform
                self.add(node)
        super().on_mouse_click(win, button, action, mods)

def main():
    
    glfw.init()
    pb.connect(pb.DIRECT)
    creator = Creator()
    
    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_tex_trans = Shader("shaders/texture_transition.vert", "shaders/texture_transition.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")
    shader_skybox = Shader("shaders/skybox.vert", "shaders/skybox.frag")
    shader_water = Shader("shaders/water.vert", "shaders/water.frag", "shaders/water.geom")
    
    shader_color_instance = Shader("shaders/color_instance.vert", "shaders/color.frag")
    shader_tex_instance = Shader("shaders/texture_instance.vert", "shaders/texture.frag")
    if len(sys.argv) >= 2:
        node = Node()
        node.children = load_scene(sys.argv[1], shader_anim, shader_tex, shader_color, shader_tex_instance, shader_color_instance, mat_parent=node.transform)
        creator.add(node)
    
    # terrain
    grass_texture = Texture("../textures/grass.png")
    sand_texture = Texture("../textures/sand.png")
    water_texture = Texture("../textures/water.png")
    
    p = Perlin2D(5, interpolation_func=smooth_interpolation, seed=0)
    chunk_grid = ChunkGrid(transform=scale(500, 100, 500))
    terrain = Terrain(shader_tex, grass_texture, 100, p.evaluate)
    for i in range(-2, 1):
        for j in range(-2, 3):
            chunk_grid.add_to_chunk(terrain, i, j)
    water = Water(shader_water, water_texture, 100)
    water_node = Node(transform=scale(1, -.5, 1))
    water_node_2 = Node(transform=scale(1, -.5, 1))
    water_node.add(water)
    water_node_2.add(water)
    chunk_grid.add_to_chunk(water_node, 2, 0, -.5)
    chunk_grid.add_to_chunk(water_node_2, 3, 0, -.5)
    beach = TerrainTransition(shader_tex_trans, grass_texture, sand_texture, 100,
                              lambda x, z: descending_slope(convert(x)) * p.evaluate(x, z),
                              lambda x, z: (x + 1) / 2)
    chunk_grid.add_to_chunk(beach, 1, 0)
    ocean_transition = Terrain(shader_tex, sand_texture, 100, lambda x, z: descending_slope(1 - convert(x)) * (
                p.evaluate(x, z) - 2 * smoothstep(convert(x))))
    chunk_grid.add_to_chunk(ocean_transition, 2, 0)
    ocean = Terrain(shader_tex, sand_texture, 100, p.evaluate)
    chunk_grid.add_to_chunk(ocean, 3, 0, -2)
    creator.add(chunk_grid)
    
    # ajout de la skybox
    creator.add(load_textured("../models/skybox/skybox.obj", shader_skybox)[0])
    
    # récupérations des objets
    creator.load("../models", shader_anim, shader_tex, shader_color)
    
    # on lance la boucle principale
    creator.run()
    pb.disconnect()
    glfw.terminate()

if __name__ == "__main__":
    main()
