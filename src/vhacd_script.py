#!/usr/bin/env python3
import pybullet as pb
import fileinput
import numpy as np
from transform import identity
from loader import load_model
from core import Shader, Node
from world import World
import glfw


def write_obj(node, file, transform=identity(), i_v=1):
    print("Writing %s to %s" % (node.__class__.__name__, file.name))
    i_in = i_v
    if not isinstance(node, Node):
        for vertice in node.vertices:
            to_write = transform @ np.array((vertice[0], vertice[1], vertice[2], 1.0))
            file.write("v %s %s %s\n" % (to_write[0], to_write[1], to_write[2]))
            i_v += 1
        for face in node.faces:
            file.write("f %s %s %s\n" % (face[0] + i_in, face[1] + i_in, face[2] + i_in))
    else:
        for n in node.children:
            i_v = write_obj(n, file, transform @ node.transform, i_v)
        if hasattr(node, 'chunks'):
            for chunk in node.chunks:
                i_v = write_obj(node.chunks[chunk], file, transform @ node.transform, i_v)
    return i_v


def convert_to_obj(file_in, file_obj, shader_anim, shader_tex, shader_color):
    """load resources from file using assimp, return node hierarchy """
    
    root_node = load_model(file_in, shader_anim, shader_tex, shader_color, save_vertices=True)[0]
    with open(file_obj, 'w') as fout:
        write_obj(root_node, fout)

def main():
    pb.connect(pb.DIRECT)
    glfw.init()

    World()
    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")

    for line in fileinput.input():
        name_fbx = line[:-1]
        name_temp = name_fbx[:-4] + ".obj"
        convert_to_obj(name_fbx, name_temp, shader_anim, shader_tex, shader_color)
        
        # name_vhacd = name_fbx[:-4] + "_vhacd.obj"
        # name_log = "log.txt"
        # pb.vhacd(name_temp, name_vhacd, name_log, resolution=100000, concavity=0, maxNumVerticesPerCH=32, minVolumePerCH=0)

    glfw.terminate()
    pb.disconnect()

if __name__ == "__main__":
    main()
