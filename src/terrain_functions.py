import numpy as np


def rotate_function(f):
	"""
	Takes a 1 dimensionnal function
	f : R+ -> R
	and returns a 2 dimensionnal function
	by rotating around the z axis
	g : R² -> R
	"""
	return lambda x, y: f(np.sqrt(x * x + y * y))


def hill(x):
	return 1 + np.cos(np.pi * x)


def sigmoid(x):
	return 1 / (1 + np.exp((x - .5) * 10))


def convert(x):
	# Converts linearly from [-1, 1] to [0, 1] and clamps
	if x < -1:
		return 0
	elif x > 1:
		return 1
	return (x + 1) / 2


def descending_slope(x):
	return (x - 1) * (x - 1)


def smoothstep(x):
	return x * x * (3 - 2 * x)


# Continuous function that evaluates 0 on the edges
def fit(x, z):
	x = 1 - abs(x)
	z = 1 - abs(z)
	if abs(x) >= 1 or abs(z) >= 1:
		return 0
	return x * x * (3 - 2 * x) * z * z * (3 - 2 * z)
	# np.exp(1 - 1 / (1 - x * x))


def global_topology(height_function, x, z):
	return height_function(x, z) \
		   + fit(x, z) * (2 * np.exp(-np.pi * (x**2 + z**2)) \
			  + 0.5*height_function(2 * x, 2 * z) \
			  + 0.25*height_function(4 * x, 4 * z) \
			  + 0.13*height_function(8 * x, 8 * z) / 1.88)
