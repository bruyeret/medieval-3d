from loader import load_model, load_animation
from transform import vec, translate, rotate
from core import Node
import numpy as np
import glfw


class State:
    def __init__(self, **kwargs):
        self.animation = None
        self.speed = 0
        self.angular_speed = 0
        for key, value in kwargs.items():
            if key == "animation_file":
                animations = load_animation(value)
                if len(animations) != 1:
                    raise AttributeError("Animation file does not contain exactly one animation")
                self.animation = animations[0]
            elif key == "speed":
                self.speed = int(value)
            elif key == "angular_speed":
                self.angular_speed = int(value)
            else:
                raise AttributeError("Wrong parameter {}".format(key))


class Entity:
    def __init__(self, model_file, shader_anim, shader_tex, shader_color, animation_speed=1):
        self.position = vec(0, 0, 0)
        self.angle = 0
        control_nodes = load_model(model_file, shader_anim, shader_tex, shader_color)
        if len(control_nodes) != 1:
            raise AttributeError("Model specified for Entity constructor does not contain exactly one mesh")
        self.control_node = control_nodes[0]
        self.control_node.set_animation_speed(animation_speed)
        self.position_node = Node()
        self.position_node.add(self.control_node)
        self.states = {}
        self.animation_start = 0
        self.animation_speed = animation_speed
        self.current_state = None
        self.target = None
        self.is_dead = False
        self.death_time = 0
        self.has_body = True

    @property
    def current_animation(self):
        if self.current_state is not None:
            return self.current_state.animation
        return None

    @property
    def speed(self):
        if self.current_state is not None:
            return self.current_state.speed
        return 0

    @property
    def angular_speed(self):
        if self.current_state is not None:
            return self.current_state.angular_speed
        return 0

    @property
    def front(self):
        return vec(np.sin(np.radians(self.angle)), 0, np.cos(np.radians(self.angle)))

    def add_state(self, state_name, **kwargs):
        state = State(**kwargs)
        self.states[state_name] = state

    def set_state(self, state_name, cancel_animation=False):
        if state_name in self.states:
            self.current_state = self.states[state_name]
        else:
            self.current_state = None
        if cancel_animation:
            self.animation_start = float("-inf")

    def kill(self, death_time):
        if not self.is_dead:
            self.is_dead = True
            self.death_time = death_time
            self.control_node.build_body()
            self.control_node.set_ragdoll_mode(True)
            self.set_state(None)

    def draw(self, model, world):
        if world.time > 8 and world.time < 9:
            self.kill(world.time)
        if self.is_dead:
            # removing body collision after 10 seconds
            if self.has_body and world.time > self.death_time + 10:
                print("BODY REMOVED")
                self.control_node.remove_body()
                self.has_body = False
            self.control_node.draw(model, world)
        else:
            self.move(world.delta_time)
            # self.advance(self.speed * world.delta_time)
            # self.rotate(180 * world.delta_time)
            self.position_node.transform = translate(*self.position)
            self.control_node.transform = rotate(vec(0, 1, 0), self.angle)
            self.position_node.draw(model, world)
            if self.current_animation is not None and \
                    glfw.get_time() - self.animation_start > self.current_animation.duration / self.animation_speed:
                self.animate()

    # To be overridden in subclasses
    # Updates the behavior of the entity, called each frame
    def update(self):
        pass

    def move(self, delta_time):
        if self.target is not None:
            x, _, z = self.target - self.position
            angle = np.angle(complex(z, x), deg=True)
            if angle < 0:
                angle += 360
            delta_angle = ((angle - self.angle) % 360)
            if delta_angle > 180:
                delta_angle -= 360
            if abs(delta_angle) < self.angular_speed * delta_time:
                self.rotate(delta_angle)
            elif delta_angle > 0:
                self.rotate(self.angular_speed * delta_time)
            else:
                self.rotate(-self.angular_speed * delta_time)
            if x ** 2 + z ** 2 < self.speed * delta_time / (2 * np.sin(np.pi * 360 / (self.angular_speed * delta_time))) ** 2:
               self.target = None
            xf, _, zf = self.front
            norm = np.sqrt(x**2 + z**2)
            if norm != 0:
                ratio = max(0, xf * x + zf * z) / norm
                self.advance(min(norm, self.speed * ratio * delta_time))
        self.update()

    # move horizontally
    def advance(self, length):
        self.position += length * self.front

    def rotate(self, angle):
        self.angle += angle
        self.angle %= 360

    def animate(self):
        if self.current_animation is not None:
            self.animation_start = glfw.get_time()
            self.control_node.animate(self.current_animation, self.animation_start)
        else:
            self.animation_start = float("-inf")

    def set_target(self, point):
        self.target = point


class Archer(Entity):
    def __init__(self, shader_anim, shader_tex, shader_color):
        folder = "../models/pack1/archer/"
        super().__init__(folder + "archer_standing.FBX", shader_anim, shader_tex, shader_color, animation_speed=.5)
        self.add_state("attack", animation_file=folder + "archer_attacking.FBX")
        self.add_state("running", animation_file=folder + "archer_running.FBX", speed=500, angular_speed=180)
        self.add_state("walking", animation_file=folder + "archer_walking.FBX", speed=200, angular_speed=180)
        self.add_state("standing", animation_file=folder + "archer_standing.FBX")
        self.set_state("standing")

    def update(self):
        if self.target is None:
            self.set_state("standing")

    def set_target(self, point):
        if self.target is None:
            self.set_state("running", True)
        self.target = point

    def key_handler(self, key):
        if __name__ == "__main__":
            if key == glfw.KEY_SPACE:
                self.set_target(world.position)


class Swordman(Entity):
    def __init__(self, shader_anim, shader_tex, shader_color):
        folder = "../models/pack1/swordman/"
        super().__init__(folder + "swordman_standing.FBX", shader_anim, shader_tex, shader_color, animation_speed=.5)
        self.add_state("attack", animation_file=folder + "swordman_standing.FBX")
        self.add_state("running", animation_file=folder + "swordman_running.FBX", speed=500, angular_speed=180)
        self.add_state("walking", animation_file=folder + "swordman_walking.FBX", speed=200, angular_speed=180)
        self.add_state("standing", animation_file=folder + "swordman_standing.FBX")
        self.set_state("standing")

    def update(self):
        if self.target is None:
            self.set_state("standing")

    def set_target(self, point):
        if self.target is None:
            self.set_state("running", True)
        self.target = point

    def key_handler(self, key):
        if __name__ == "__main__":
            if key == glfw.KEY_SPACE:
                self.set_target(world.position)


if __name__ == "__main__":
    import glfw
    from world import World
    from core import Shader
    from loader import load_textured
    import pybullet as pb

    glfw.init()                # initialize window system glfw

    pb.connect(pb.DIRECT)
    world = World()

    shader_anim = Shader("shaders/skinning.vert", "shaders/texture.frag")
    shader_tex = Shader("shaders/texture.vert", "shaders/texture.frag")
    shader_color = Shader("shaders/color.vert", "shaders/color.frag")
    shader_normals = Shader("shaders/normals.vert", "shaders/normals.frag")
    shader_water = Shader("shaders/water.vert", "shaders/water.frag", "shaders/water.geom")
    shader_tex_trans = Shader("shaders/texture_transition.vert", "shaders/texture_transition.frag")
    shader_skybox = Shader("shaders/skybox.vert", "shaders/skybox.frag")

    world.add(load_textured("../models/skybox/skybox.obj", shader_skybox)[0])

    archer = Archer(shader_anim, shader_tex, shader_color)
    archer.set_state("running")
    world.add(archer)

    # On lance la simulation
    world.run()

    glfw.terminate()           # destroy all glfw windows and GL contexts
